//--------------------------------------------------//
#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/uaccess.h>
#include <linux/version.h>

#include <linux/types.h>
#include "lcd_mod.h"

//--------------------------------------------------//
#define COLOR_BLACK 0x0000
#define COLOR_BLUE 0x001F
#define COLOR_RED 0xF800
#define COLOR_GREEN 0x07E0
#define COLOR_CYAN 0x07FF
#define COLOR_MAGENTA 0xF81F
#define COLOR_YELLOW 0xFFE0
#define COLOR_WHITE 0xFFFF
//--------------------------------------------------//

#define DEVICE_FIRST 0
#define DEVICE_COUNT 1
#define MODNAME "my_dyndev_mod"
#define PROC_DIRECTORY "task06"
#define PROC_FILENAME "buffer_info"
#define DEVNAME "msg0"

#define CLASS_ATTR(_name, _mode, _show, _store)                                \
	struct class_attribute class_attr_##_name =                            \
		__ATTR(_name, _mode, _show, _store)

//--------------------------------------------------//
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Yurii Osipchuk");
MODULE_DESCRIPTION("Task06");
MODULE_VERSION("6.1");
//--------------------------------------------------//
static int buffer_size = 1024;
static const int buffer_size_max = 1024;
static const size_t messages_count = 0;
static uint32_t sysfs_user_bg_color = COLOR_BLACK;
static uint32_t sysfs_user_text_color = COLOR_BLUE;

// sysfs_user_font:
// 0 = Font_7x10;
// 1 = Font_11x18;
// 2 = Font_16x26;
static int sysfs_user_font = 1;
static char sysfs_user_name[64] = "";
static char *dev_buffer = NULL;

static int major = 0;

static struct cdev hcdev;
static struct class *devclass;
static struct proc_dir_entry *proc_dir;
static struct proc_dir_entry *proc_file;

//--------------------------------------------------//
module_param(major, int, S_IRUGO);
module_param(buffer_size, int, S_IRUGO);

//--------------------------------------------------//
static ssize_t dev_read(struct file *file, char *user_buffer, size_t count,
			loff_t *offset);
static ssize_t dev_write(struct file *file, const char __user *user_buffer,
			 size_t count, loff_t *offset);
static int create_proc(void);
static void cleanup_proc(void);
static ssize_t proc_fs_read(struct file *file, char *buf, size_t count,
			    loff_t *ppos);
// value
static ssize_t sys_read_value(struct class *class, struct class_attribute *attr,
			      char *buf);
static ssize_t sys_write_value(struct class *class,
			       struct class_attribute *attr, const char *buf,
			       size_t count);
// user
static ssize_t sys_read_user(struct class *class, struct class_attribute *attr,
			     char *buf);
static ssize_t sys_write_user(struct class *class, struct class_attribute *attr,
			      const char *buf, size_t count);
static ssize_t sys_read_text_color(struct class *class,
				   struct class_attribute *attr, char *buf);
static ssize_t sys_write_text_color(struct class *class,
				    struct class_attribute *attr,
				    const char *buf, size_t count);
static ssize_t sys_read_bg_color(struct class *class,
				 struct class_attribute *attr, char *buf);
static ssize_t sys_write_bg_color(struct class *class,
				  struct class_attribute *attr, const char *buf,
				  size_t count);
static ssize_t sys_read_font(struct class *class, struct class_attribute *attr,
			     char *buf);
static ssize_t sys_write_font(struct class *class, struct class_attribute *attr,
			      const char *buf, size_t count);
static int create_buffer(void);
static void free_buffer(void);
static void clear_buffer(void);
int str_to_int(const char *str);
size_t count_messages(const char *str);
void print_str(char *str, size_t count);
uint32_t color_color565(const char *str);
FontDef int_font(int num);
//--------------------------------------------------//
// cdev
static const struct file_operations dev_fops = {
	.owner = THIS_MODULE,
	.read = dev_read,
	.write = dev_write,
};

#ifdef NOTQUEM
static const struct file_operations proc_fops = { .read = proc_fs_read };
#endif

#ifdef QUEM
static struct proc_ops proc_fops = {
	.proc_read = proc_fs_read,
};
#endif

//--------------------------------------------------//
// Other helps function
int str_to_int(const char *str)
{
	size_t tmp, count = 0;
	while (*str) {
		if (*str == ' ' || *str == '\t' || *str == '\n') {
			str++;
			continue;
		}

		if (*str >= 48 && *str <= 57) {
			tmp = *str - 48;
			if (count != 0) {
				count = count * 10 + tmp;
			} else {
				count = tmp;
			}
		}
		str++;
	}
	return count;
}

void print_str(char *str, size_t count)
{
	size_t i;
	printk(KERN_INFO "print_str:\n");
	for (i = 0; i < count; ++i) {
		printk(KERN_INFO "%02d", str[i]);
	}
	printk(KERN_INFO "\n");
}

size_t count_messages(const char *str)
{
	size_t count = 0;
	size_t i;
	size_t bytes = strlen(str);
	char ch = '\n'; // 10

	for (i = 0; i < bytes; ++i) {
		if (str[i] == ch && i != 0) {
			++count;
		}
	}

	return count;
}

uint32_t color_color565(const char *str)
{
	uint32_t ret = 0, tmp = 0, count = 0;
	while (*str) {
		if (*str == ' ' || *str == '\t' || *str == '\n') {
			str++;
			continue;
		}
		if (*str == ',') {
			if (count != 0) {
				if (ret == 0) {
					ret = ((count & 0xF8) << 8);
				} else {
					ret |= ((count & 0xFC) << 3);
				}
				count = 0;
			}
			str++;
			continue;
		}

		if (*str >= 48 && *str <= 57) {
			tmp = *str - 48;
			if (count != 0) {
				count = count * 10 + tmp;
			} else {
				count = tmp;
			}
		}
		str++;
	}
	if (count != 0) {
		ret |= ((count & 0xF8) >> 3);
	}

	return ret;
}

FontDef int_font(int num)
{
	if (num == 0) {
		return Font_7x10;
	} else if (num == 1) {
		return Font_11x18;
	} else if (num == 2) {
		return Font_16x26;
	}
	return Font_11x18;
}

//--------------------------------------------------//
// function for dev_buffer
static int create_buffer(void)
{
	char *temp_bufer = NULL;
	size_t count;
	if (buffer_size > buffer_size_max) {
		buffer_size = buffer_size_max;
	}

	temp_bufer = kmalloc(buffer_size, GFP_KERNEL);
	if (NULL == temp_bufer) {
		return -ENOMEM;
	}
	memset(temp_bufer, '\0', buffer_size);

	if (dev_buffer != NULL) {
		count = strlen(dev_buffer);
		count = count > buffer_size ? buffer_size : count;
		strncpy(temp_bufer, dev_buffer, count - 1);
		temp_bufer[count] = '\0';
		kfree(dev_buffer);
	}
	dev_buffer = temp_bufer;
	temp_bufer = NULL;
	printk(KERN_INFO "create_buffer(): buffer_size = %d\n", buffer_size);
	return 0;
}

static void free_buffer(void)
{
	if (dev_buffer) {
		kfree(dev_buffer);
		dev_buffer = NULL;
		printk(KERN_INFO "free_buffer(): ok\n");
	}
}

static void clear_buffer(void)
{
	memset(dev_buffer, '\0', buffer_size);
	printk(KERN_INFO "clear_buffer(): ok\n");
}

static void add_user_buffer(void)
{
	size_t i;
	size_t len_buff = strlen(dev_buffer);
	size_t len_user = strlen(sysfs_user_name);
	if ((buffer_size - len_buff - 2) > len_user && len_user > 0) {
		// strcat(dev_buffer, sysfs_user_name);
		for (i = 0; i < len_user; ++i) {
			if (sysfs_user_name[i] == '\n') {
				continue;
			}
			dev_buffer[len_buff] = sysfs_user_name[i];
			++len_buff;
		}
		dev_buffer[len_buff] = ':';
		dev_buffer[len_buff + 1] = '\0';
	}
}
//--------------------------------------------------//
//--------------------------------------------------//
// CDEV
static ssize_t dev_read(struct file *file, char *user_buffer, size_t count,
			loff_t *offset)
{
	int retval = 0;
	size_t bytes;

	bytes = strlen(dev_buffer) - (*offset);
	printk(KERN_INFO "=== to user read buffer len : %ld\n",
	       (long)strlen(dev_buffer));
	printk(KERN_INFO "=== to user offset: %ld\n", (long)*offset);

	if (bytes) {
		retval = copy_to_user(user_buffer, dev_buffer, bytes);
		if (!retval) {
			*offset = bytes;
			printk(KERN_INFO "=== to user offset after: %ld\n",
			       (long)*offset);
			printk(KERN_INFO "=== to user read retval true: %d\n",
			       retval);
		} else {
			printk(KERN_INFO "=== to user read retval false: %d\n",
			       retval);
			return retval;
		}
	}
	return bytes;
}

static ssize_t dev_write(struct file *file, const char __user *user_buffer,
			 size_t count, loff_t *offset)
{
	int retval = 0;
	size_t bytes = 0;
	size_t i;
	char ch = 0;
	add_user_buffer();
	size_t len = strlen(dev_buffer);

	printk(KERN_INFO "=== from user buffer len : %ld\n", (long)len);
	printk(KERN_INFO "=== bytes from user : %ld\n", (long)count);

	for (i = 0; i < count; ++i) {
		retval = copy_from_user(&ch, user_buffer + i, 1);
		if ((buffer_size - 1 - len) > 0) {
			// strcat(dev_buffer, &ch);
			dev_buffer[len] = ch;
			++len;
			++bytes;
		} else {
			if (dev_buffer[len + 1] != '\0') {
				dev_buffer[len + 1] = '\0';
			}
		}
	}
	if (retval) {
		return retval;
	}
	printk(KERN_INFO "=== bytes from user read : %ld\n", (long)bytes);
	return count;
}

//--------------------------------------------------//
// PROCFS
static int create_proc(void)
{
	proc_dir = proc_mkdir(PROC_DIRECTORY, NULL);
	if (NULL == proc_dir) {
		printk(KERN_ERR "=== Can not create procfs directory\n");
		return -EFAULT;
	}

	proc_file = proc_create(PROC_FILENAME, S_IFREG | S_IRUGO | S_IWUGO,
				proc_dir, &proc_fops);
	if (NULL == proc_file) {
		printk(KERN_ERR "=== Can not create procfs file\n");
		return -EFAULT;
	}
	return 0;
}

static void cleanup_proc(void)
{
	if (proc_file) {
		remove_proc_entry(PROC_FILENAME, proc_dir);
		printk(KERN_INFO "=== Cleanup procfs file\n");
		proc_file = NULL;
	}
	if (proc_dir) {
		remove_proc_entry(PROC_DIRECTORY, NULL);
		printk(KERN_INFO "=== Cleanup procfs directory\n");
		proc_dir = NULL;
	}
}

static ssize_t proc_fs_read(struct file *file, char *buf, size_t count,
			    loff_t *ppos)
{
	int len;
	char proc_buff[128];
	sprintf(proc_buff,
		"buffer size: %d\ncount messages: %ld\nfree space: %ld\nfont: %d\nbg color: 0x%04x\ntext color: 0x%04x\n",
		buffer_size, (long)count_messages(dev_buffer),
		(long)(buffer_size - strlen(dev_buffer) - 1), sysfs_user_font,
		sysfs_user_bg_color, sysfs_user_text_color);
	len = strlen(proc_buff);
	if (count < len)
		return -EINVAL;
	if (*ppos != 0) {
		printk(KERN_INFO "=== procfs read return : 0\n"); // EOF
		return 0;
	}

	lcd_fill_rectangle(0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1,
			   (u16)sysfs_user_bg_color);
	lcd_put_str_nline(5, 50, proc_buff, int_font(sysfs_user_font),
			  (u16)sysfs_user_text_color, (u16)sysfs_user_bg_color);
	lcd_update_screen();

	printk(KERN_INFO "%s", proc_buff);
	if (copy_to_user(buf, proc_buff, len))
		return -EINVAL;
	*ppos = len;
	return len;
}

//--------------------------------------------------//
//--------------------------------------------------//
// SYSFS
// to userspace
static ssize_t sys_read_value(struct class *class, struct class_attribute *attr,
			      char *buf)
{
	sprintf(buf, "%d\n", buffer_size);
	printk(KERN_INFO "sysfs read value: %d\n", buffer_size);
	return strlen(buf);
}

// from usercpace
static ssize_t sys_write_value(struct class *class,
			       struct class_attribute *attr, const char *buf,
			       size_t count)
{
	buffer_size = str_to_int(buf);
	create_buffer();
	printk(KERN_INFO "sysfs write value: %d\n", buffer_size);
	return count;
}

CLASS_ATTR(value, (S_IWUSR | S_IRUGO), &sys_read_value, &sys_write_value);

// to userspace
static ssize_t sys_read_user(struct class *class, struct class_attribute *attr,
			     char *buf)
{
	strcpy(buf, sysfs_user_name);
	printk(KERN_INFO "sysfs read user: %s", sysfs_user_name);
	return strlen(buf);
}

// from usercpace
static ssize_t sys_write_user(struct class *class, struct class_attribute *attr,
			      const char *buf, size_t count)
{
	memset(sysfs_user_name, '\0', strlen(sysfs_user_name));
	strncpy(sysfs_user_name, buf, count);
	printk(KERN_INFO "sysfs write user: %s", sysfs_user_name);
	return count;
}

CLASS_ATTR(user, (S_IWUSR | S_IRUGO), &sys_read_user, &sys_write_user);

// to userspace
static ssize_t sys_read_text_color(struct class *class,
				   struct class_attribute *attr, char *buf)
{
	sprintf(buf, "text color: 0x%04x\n", sysfs_user_text_color);
	printk(KERN_INFO "read text color: 0x%04x\n", sysfs_user_text_color);
	return strlen(buf);
}

// from usercpace
static ssize_t sys_write_text_color(struct class *class,
				    struct class_attribute *attr,
				    const char *buf, size_t count)
{
	sysfs_user_text_color = color_color565(buf);
	printk(KERN_INFO "write text color: %s", buf);
	return count;
}

CLASS_ATTR(text_color, (S_IWUSR | S_IRUGO), &sys_read_text_color,
	   &sys_write_text_color);

// to userspace
static ssize_t sys_read_bg_color(struct class *class,
				 struct class_attribute *attr, char *buf)
{
	sprintf(buf, "bg color: 0x%04x\n", sysfs_user_bg_color);
	printk(KERN_INFO "read bg color: 0x%04x\n", sysfs_user_bg_color);
	return strlen(buf);
}

// from usercpace
static ssize_t sys_write_bg_color(struct class *class,
				  struct class_attribute *attr, const char *buf,
				  size_t count)
{
	sysfs_user_bg_color = color_color565(buf);
	printk(KERN_INFO "write bg color: %s", buf);
	return count;
}

CLASS_ATTR(bg_color, (S_IWUSR | S_IRUGO), &sys_read_bg_color,
	   &sys_write_bg_color);

// to userspace
static ssize_t sys_read_font(struct class *class, struct class_attribute *attr,
			     char *buf)
{
	sprintf(buf, "text font: %d\n", sysfs_user_font);
	printk(KERN_INFO "read text font: %d\n", sysfs_user_font);
	return strlen(buf);
}

// from usercpace
static ssize_t sys_write_font(struct class *class, struct class_attribute *attr,
			      const char *buf, size_t count)
{
	int font_num = str_to_int(buf);
	if (font_num >= 0 && font_num < 3) {
		sysfs_user_font = font_num;
	} else {
		sysfs_user_font = 1;
	}

	printk(KERN_INFO "write text font: %s", buf);
	return count;
}

CLASS_ATTR(font, (S_IWUSR | S_IRUGO), &sys_read_font, &sys_write_font);

//--------------------------------------------------//
static int __init dev_init(void)
{
	int ret, i;
	dev_t dev;

	if (major != 0) {
		// статичный major
		dev = MKDEV(major, DEVICE_FIRST);
		ret = register_chrdev_region(dev, DEVICE_COUNT, MODNAME);
		printk(KERN_INFO "register_chrdev_region: %d", ret);
	} else {
		// динамичный major
		ret = alloc_chrdev_region(&dev, DEVICE_FIRST, DEVICE_COUNT,
					  MODNAME);
		printk(KERN_INFO "alloc_chrdev_region: %d", ret);
		major = MAJOR(dev); // не забыть зафиксировать!
		printk(KERN_INFO "major: %d", major);
	}

	if (ret < 0) {
		printk(KERN_ERR "=== Can not register char device region\n");
		goto err;
	}

	cdev_init(&hcdev, &dev_fops);
	hcdev.owner = THIS_MODULE;
	ret = cdev_add(&hcdev, dev, DEVICE_COUNT);
	if (ret < 0) {
		unregister_chrdev_region(MKDEV(major, DEVICE_FIRST),
					 DEVICE_COUNT);
		printk(KERN_ERR "=== Can not add char device\n");
		goto err;
	}

	// struct class*
	devclass = class_create(THIS_MODULE, "task06");
	for (i = 0; i < DEVICE_COUNT; i++) {
		dev = MKDEV(major, DEVICE_FIRST + i);
		device_create(devclass, NULL, dev, NULL, "%s%d", DEVNAME,
			      i + 1);
	}

	printk(KERN_INFO "======== module installed %d:[%d-%d] ===========\n",
	       MAJOR(dev), DEVICE_FIRST, MINOR(dev));

	ret = class_create_file(devclass, &class_attr_value); // sys_fs value
	if (ret) {
		printk(KERN_ERR "=== Can not class create file value\n");
		goto err;
	}

	ret = class_create_file(devclass, &class_attr_user); // sys_fs user
	if (ret) {
		printk(KERN_ERR "=== Can not class create file user\n");
		goto err;
	}

	ret = class_create_file(devclass,
				&class_attr_text_color); // sys_fs text color
	if (ret) {
		printk(KERN_ERR "=== Can not class create file text color\n");
		goto err;
	}

	ret = class_create_file(devclass,
				&class_attr_bg_color); // sys_fs bg color
	if (ret) {
		printk(KERN_ERR "=== Can not class create file bg color\n");
		goto err;
	}

	ret = class_create_file(devclass, &class_attr_font); // sys_fs font
	if (ret) {
		printk(KERN_ERR "=== Can not class create file font\n");
		goto err;
	}

	ret = create_proc(); // proc_fs value
	if (ret) {
		printk(KERN_ERR "=== Can not create procfs\n");
		goto err;
	}

	create_buffer();

	ret = start_func();
	if (ret != 0) {
		goto err;
	}

	return 0;

err:
	cleanup_proc();
	free_buffer();
	return ret;
}
//--------------------------------------------------//
static void __exit dev_exit(void)
{
	dev_t dev;
	int i;

	cleanup_proc(); // proc_fs
	class_remove_file(devclass, &class_attr_value); // sys_fs value
	class_remove_file(devclass, &class_attr_user); // sys_fs user
	class_remove_file(devclass, &class_attr_text_color); // sys_fs user

	for (i = 0; i < DEVICE_COUNT; i++) {
		dev = MKDEV(major, DEVICE_FIRST + i);
		device_destroy(devclass, dev);
	}
	class_destroy(devclass);
	cdev_del(&hcdev);
	unregister_chrdev_region(MKDEV(major, DEVICE_FIRST), DEVICE_COUNT);

	free_buffer();

	exit_func();

	printk(KERN_INFO "=============== module removed ================\n");
}
//--------------------------------------------------//
module_init(dev_init);
module_exit(dev_exit);
//--------------------------------------------------//
