#!/bin/bash

MYPATH=$(pwd)
filesList=$(find $MYPATH -mtime +30 -type f)

for i in $filesList; do
  echo $i;
  FILENAME=$(basename $i);
  echo "MYPATH = $MYPATH";
  mv "$MYPATH/$FILENAME" "$MYPATH/~$FILENAME"
done

DELFILE=$(find $MYPATH -type f -name "~*")
if [[ -n $DELFILE ]]; then
  echo "ok";
  $(bash exercise01.sh);
fi
