#ifndef __LCD_MOD_H__
#define __LCD_MOD_H__

#include <linux/init.h>
#include <linux/module.h>
#include <linux/spi/spi.h>
#include <linux/delay.h>
#include <linux/gpio.h>
#include <linux/device.h>
#include "ili9341.h"
#include "fonts.h"

#define DATA_SIZE 90

static u16 frame_buffer[LCD_WIDTH * LCD_HEIGHT];
static struct spi_device *lcd_spi_device;

static void lcd_reset(void);
static void lcd_write_command(u8 cmd);
static void lcd_write_data(u8 *buff, size_t buff_size);
static void lcd_set_address_window(u16 x0, u16 y0, u16 x1, u16 y1);
inline void lcd_update_screen(void);
void lcd_draw_pixel(u16 x, u16 y, u16 color);
void lcd_fill_rectangle(u16 x, u16 y, u16 w, u16 h, u16 color);
void lcd_fill_screen(u16 color);
static void lcd_put_char(u16 x, u16 y, char ch, FontDef font, u16 color,
			 u16 bgcolor);
void lcd_init_ili9341(void);
void lcd_put_str(u16 x, u16 y, const char *str, FontDef font, u16 color,
		 u16 bgcolor);
void lcd_put_str_nline(u16 x, u16 y, const char *str, FontDef font, u16 color,
		       u16 bgcolor);
void exit_func(void);
int start_func(void);

#endif // __LCD_MOD_H__
