#!/bin/bash
pin=26
count=0
button_detect=0

trap "echo ${pin} > /sys/class/gpio/unexport; exit" SIGINT

echo ${pin} > /sys/class/gpio/export
echo in > /sys/class/gpio/gpio${pin}/direction

# while [ $count -lt 50 ]; do
while true; do
    button_detect=$(cat /sys/class/gpio/gpio${pin}/value)
    while [ $button_detect -eq 0 ]; do
    button_detect=$(cat /sys/class/gpio/gpio${pin}/value)
    done

    #sleep 0.1
    button_detect=$(cat /sys/class/gpio/gpio${pin}/value)
    if [ $button_detect -eq 1 ]; then
            count=$(( $count + 1 ))
            echo "count: $count"
    fi
    
    while [ $button_detect -eq 1 ]; do
            button_detect=$(cat /sys/class/gpio/gpio${pin}/value)
    done
done

#echo ${pin} > /sys/class/gpio/unexport
