#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/uaccess.h>
// #include <linux/slab.h>
#include <linux/cdev.h>
#include <linux/version.h>
#include <linux/jiffies.h>
#include <linux/types.h>
#include <linux/time.h>

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Yurii Osipchuk");
MODULE_DESCRIPTION("Task07");
MODULE_VERSION("1.0");

#define DIRECTORY "tassk07"

#define CLASS_ATTR(_name, _mode, _show, _store)                                \
	struct class_attribute class_attr_##_name =                            \
		__ATTR(_name, _mode, _show, _store)

static u32 time_start;
static struct class *sys_class;

static ssize_t sys_read(struct class *class, struct class_attribute *attr,
			char *buf)
{
	u32 time_end, temp;

	time_end = jiffies;
	if (time_start == 0 || time_end < time_start) {
		temp = 0;
	} else {
		temp = time_end - time_start;
	}
	time_start = jiffies;

	printk(KERN_INFO "seconds = %d\n", temp / HZ);
	sprintf(buf, "seconds = %d\n", temp / HZ);

	return strlen(buf);
}

static ssize_t sys_write(struct class *class, struct class_attribute *attr,
			 const char *buf, size_t count)
{
	return 0;
}

CLASS_ATTR(value, (S_IWUSR | S_IRUGO), &sys_read, &sys_write);

static int __init example_init(void)
{
	int err;
	time_start = 0;

	sys_class = class_create(THIS_MODULE, DIRECTORY);
	if (IS_ERR(sys_class))
		printk("bad class create\n");
	err = class_create_file(sys_class, &class_attr_value);
	if (err)
		goto error;

	printk(KERN_NOTICE "loaded\n");
	return 0;

error:
	printk(KERN_ERR "failed to load\n");
	class_remove_file(sys_class, &class_attr_value);
	class_destroy(sys_class);
	return err;
}

static void __exit example_exit(void)
{
	class_remove_file(sys_class, &class_attr_value);
	class_destroy(sys_class);
	printk(KERN_NOTICE "exited\n");
}

module_init(example_init);
module_exit(example_exit);
