# Author
Yurii Osipchuk
# Description
[Task #13](https://gl-khpi.gitlab.io/exercises/exercise13/).

## Time Management:

### 1. tick.c:
![Image01](res/tick.png)

### 2. interv.c:
![Image02](res/interv.png)

### 3. hrtimer:
![Image03](res/hrtimer.png)

### 4. mytimer.c:
![Image04](res/mytimer.png)

