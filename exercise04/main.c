#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "list.h"

char *info(int a, int b)
{
	if (a > b) {
		return "player1 win";
	} else if (a < b) {
		return "player2 win";
	} else {
		return "player1 == player2";
	}
}

int main(int argc, char *argv[])
{
	struct Node *plist = NULL;
	push_list(&plist, 1, "player1");
	push_list(&plist, 2, "player2");
	int i, arg;

	if (argc == 2) {
		arg = atoi(argv[1]);
	} else {
		arg = 1;
	}

	for (i = 0; i < arg; ++i) {
		set_list(plist, 1, rand() % 6 + 1);
		set_list(plist, 2, rand() % 6 + 1);
	}

	printf("%s = %d and %s = %d: %s\n", get_list_name(plist, 1),
	       get_list_result(plist, 1), get_list_name(plist, 2),
	       get_list_result(plist, 2),
	       info(get_list_result(plist, 1), get_list_result(plist, 2)));

	close_list(plist);

	return 0;
}