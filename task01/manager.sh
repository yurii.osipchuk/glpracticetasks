#!/bin/bash

MYPATH=$(pwd)
VIEW_FORMAT=""
SCRIPT_EXIT=0

help()
{
  echo "Usage: $0"
  echo "Usage: $0 [OPTION]"
  echo "Options:"
  echo -e "\t-h\t- display this help and  exit"
  echo -e "\t-p\t- change path( -p <path> )"
  echo -e "\t-v\t- view( -v <arg> ):"
  echo -e "\t\t  1) full (default)"
  echo -e "\t\t  2) fulllist"
  echo -e "\t\t  3) short"
  echo -e "\t\t  4) shortlist"
  echo -e "\t\t  5) file"
  echo -e "\t\t  6) dir"
  echo -e "\t\t  7) sort"
  echo -e "\t\t  8) rsort"
}

help_manager()
{
  echo "Command:"
  echo -e "\t1) c - copy file"
  echo -e "\t2) d - delete file"
  echo -e "\t3) e - exit from script"
  echo -e "\t4) m - rename/move file"
  echo -e "\t5) p - change path"
  echo -e "\t6) v - view"

}

set_format()
{
	case $1 in
		"1"|"full") FIND_FILE="-1ap --color=always"; VIEW_FORMAT="full";;
		"2"|"fulllist") FIND_FILE="-lap --color=always"; VIEW_FORMAT="fulllist";;
		"3"|"short") FIND_FILE="-1p --color=always"; VIEW_FORMAT="short";;
		"4"|"shortlist") FIND_FILE="-lp --color=always"; VIEW_FORMAT="shortlist";;
		"5"|"file") FIND_FILE="-1A --color=always"; VIEW_FORMAT="file";;
		"6"|"dir") FIND_FILE="-1A --color=always"; VIEW_FORMAT="dir";;
		"7"|"sort") FIND_FILE="-lap --color=always"; VIEW_FORMAT="sort";;
		"8"|"rsort") FIND_FILE="-lrap --color=always"; VIEW_FORMAT="rsort";;
		*) FIND_FILE="-1ap --color=always"; VIEW_FORMAT="full";;
	esac
	echo "VIEW_FORMAT = $VIEW_FORMAT"

}

show_dir()
{
	clear
	echo "***** Files in folder: ******";
	echo "Your folder:"
	echo "$MYPATH"
	echo "*****************************";


	if [[ $VIEW_FORMAT = "dir" ]]; then
		find $MYPATH -maxdepth 1 -type d;
	elif [[ $VIEW_FORMAT = "file" ]]; then
		find $MYPATH -maxdepth 1 -type f;
	else
		ls $MYPATH $FIND_FILE
	fi

    echo "************ END ************";
}



if [[ -z $* ]]; then
	# echo "No options found!"
	set_format
	show_dir
else
	while getopts ":hp:v:" opt; do
	case $opt in
		h)  help; exit 0;;
		p)  cd $OPTARG; MYPATH=$(pwd);;
		v)  set_format $OPTARG;;
		*)  echo "No reasonable options found!"; exit 1;;
	esac
	done
	show_dir
fi

manager_path()
{
	read -p "Enter new path: " ARG1
	cd $ARG1
	# MYPATH=$ARG1
	MYPATH=$(pwd)
	show_dir
}

help_view()
{
	echo "View options:"
	echo "1) full"
	echo "2) fulllist"
	echo "3) short"
	echo "4) shortlist"
	echo "5) file"
	echo "6) dir"
	echo "7) sort"
	echo "8) rsort"
}

manager_view()
{
	clear
	help_view
	read -p "Enter new view: " ARG1
	set_format $ARG1
	show_dir
}

manager_copy()
{
	echo "***** Files Copy: ***********";
	read -p "Enter file name: " ARG1
	read -p "Enter directory name: " ARG2
	cp $MYPATH/$ARG1 $ARG2
	show_dir
}

manager_delete()
{
	echo "***** Files Delete: *********";
	read -p "Enter file name: " ARG1
	rm $MYPATH/$ARG1
	show_dir
}

manager_rename()
{
	echo "***** Files Remane: *********";
	read -p "Enter file name: " ARG1
	read -p "Enter new name: " ARG2
	mv $MYPATH/$ARG1 $MYPATH/$ARG2
	show_dir
}

menu()
{
	case $ARG1 in
		"1"|"c") manager_copy;;
		"2"|"d") manager_delete;;
		"3"|"e") SCRIPT_EXIT=1;;
		"4"|"m") manager_rename;;
		"5"|"p") manager_path;;
		"6"|"v") manager_view ;;
		*) echo;;
	esac
}

while [ $SCRIPT_EXIT -eq 0 ]; do
	# clear;
	menu;
	if [ $SCRIPT_EXIT -eq 0 ]; then
		echo "***** Files Manager: ********";
		help_manager
		read -p "Enter command: " ARG1
		echo;
	fi
done

echo "***** Files Manager End: ****";
