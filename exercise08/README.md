# Author
Yurii Osipchuk
# Description
[Exercise #8](https://gl-khpi.gitlab.io/exercises/exercise08/).


## Getting the kernel sources

```bash
git clone git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable -b v5.6.12 --depth 1
wget https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.6.12.tar.xz
tar -xJf linux-5.6.12.tar.xz
```

## Configuring

```bash
cd linux-5.6.12
mkdir linux_build
export BUILD_KERNEL=$(pwd)/linux_build
make O=${BUILD_KERNEL} i386_defconfig
```

## Building kerne

```bash
cd ${BUILD_KERNEL}
make menuconfig
make -j4
# Built images are located in ${BUILD_KERNEL}/arch/i386/boot/bzImage
# if don't work menuconfig:
# sudo apt install libncurses5-dev
```

## Building rootfs

```bash
git clone git://git.buildroot.net/buildroot
cd buildroot
mkdir build_rootfs
export BUILD_ROOTFS=$(pwd)/build_rootfs
make O=${BUILD_ROOTFS} qemu_x86_defconfig
cd ${BUILD_ROOTFS}
make menuconfig

echo "user 1000 user 1000 =pass /home/user /bin/bash - Linux User" > ${BUILD_ROOTFS}/users
mkdir -p ${BUILD_ROOTFS}/root/etc/sudoers.d
echo "user ALL=(ALL) ALL" > ${BUILD_ROOTFS}/root/etc/sudoers.d/user

mkdir -p ${BUILD_ROOTFS}/root/etc
echo "/bin/sh" > ${BUILD_ROOTFS}/root/etc/shells
echo "/bin/bash" >> ${BUILD_ROOTFS}/root/etc/shells
cd ${BUILD_ROOTFS}
make
```
```text
Buildroot configuration:

Target options:
	- Target Architecture = i386
	- Target Architecture Variant = i686
Toolchain:
	- Custom kernel headers series = 4.13.x (should match the kernel)
	- [*] Enable WCHAR support
System configuration:
	- System hostname = myLinux (give it a name)
	- System banner = Welcome to myLinux
	- [*] Enable root login with password
	- Root password = <rootpass>
	- Path to the users tables = ${BUILD_ROOTFS}/users (we'll create regular user)
	- Root filesystem overlay directories = ${BUILD_ROOTFS}/root (we'll put there some additional configs)
Kernel:
	- [ ] Linux Kernel (we'll use manually built kernel, so disable it here)
Target packages:
	- [*] Show packages that are also provided by busybox
	- Development tools
		[*] binutils
		[*] binutils binaries
		[*] findutils
		[*] grep
		[*] sed
		[*] tree
	- Libraries:
		Compression and decompression:
			[*] zlib
		Text and terminal handling:
			[*] ncurses
			[*] readline
	- Networking applications:
		[*] dropbear (ssh server)
		[*] wget
	- Shell and utilities:
		[*] bash
		[*] file
		[*] sudo
		[*] which
	- System tools
		[*] kmod
		[*] kmod utilities
		[*] rsyslog
	- Text editors and viewers:
		[*] joe (it might be the easiest terminal editor unless you're familiar with vi)
		[*] less
		[*] mc
		[*] vim
Filesystem images:
	[*] ext2/3/4 root filesystem
		ext2/3/4 variant = ext3
	[*] tar the root filesystem
```

## Launching

```bash
qemu-system-i386 -kernel ${BUILD_KERNEL}/arch/i386/boot/bzImage -append "root=/dev/sda console=ttyS0" \
-drive format=raw,file=${BUILD_ROOTFS}/images/rootfs.ext3 -redir tcp:8022::22 &
```
```text
Connect to ssh:
ssh -p 8022 user@localhost
scp -P 8022 ex01.ko user@localhost:/home/user
user password = pass
```
