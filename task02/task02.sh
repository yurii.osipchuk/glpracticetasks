#!/bin/bash

NUMBER=$1
UPPER_BORDER=$2
RETRIES=$3
SCRIPT_EXIT=0
GAME_EXIT=0
RNUMBER=0
CHOICE=0
WIN="WIN"

MY_RANDOM()
{
	if [ -z $1 ]; then
		RNUMBER=$((RANDOM%6))
	else
		RNUMBER=$((RANDOM%$1+1))
	fi
}

compare()
{
	if [ ! -z $3 ]; then
		echo "You have $3 lifes";
	fi

	if [[ $1 -eq $2 ]]; then
		echo "$1 is equal to X";
	elif [[ $1 -lt $2 ]]; then
		echo "$1 is less than X";
	elif [[ $1 -gt $2 ]]; then
		echo "$1 is greater than X";
	fi
}

print_help()
{
	echo "Commands:"
	echo -e "\ts, start - start game"
	echo -e "\t-r, --retries - enter count retries"
	echo -e "\t-u, --upper - enter upper border"
	echo -e "\t-n, --number - enter new number"
	echo -e "\te, exit - exit game"
}

check_param()
{
	if [[ -z $NUMBER ]]; then
		NUMBER=0
	fi

	if [[ -z $UPPER_BORDER ]]; then
		UPPER_BORDER=0
	fi

	if [[ -z $RETRIES ]]; then
		RETRIES=0
	fi
}


show_param()
{
	echo "Your enter:"
	if [[ $GAME_EXIT -eq 0 ]]; then
		echo "NUMBER = $NUMBER | PPER_BORDER = $UPPER_BORDER | RETRIES = $RETRIES"
	else
		echo "NUMBER = $NUMBER | PPER_BORDER = $UPPER_BORDER | RETRIES = $RETRIES"
		echo "YOUR $WIN"
	fi
}

apdade_view()
{
	clear
	show_param
	print_help
}

game()
{
	GAME_EXIT=0

	if [[ $NUMBER -eq 0 ]]; then
		read -rp "Enter number: " NUMBER
	fi
	apdade_view

	if [[ $UPPER_BORDER -eq 0 ]]; then
		read -rp "Enter uppper border 1 - 100: " UPPER_BORDER
	fi
	apdade_view

	if [[ $RETRIES -eq 0 ]]; then
		read -rp "Enter retries: " RETRIES
	fi
	apdade_view
	
	COUNT=$RETRIES
	MY_RANDOM $UPPER_BORDER
	# echo "RNUMBER = $RNUMBER"
	echo "***** Start Game: *********";
	while [ $GAME_EXIT -eq 0 ]; do
		compare $NUMBER $RNUMBER $COUNT
		if [[ $NUMBER -eq $RNUMBER ]]; then
			GAME_EXIT=1
			WIN="WIN"
		else
			if [[ $COUNT -ne 0 ]]; then
				read -rp "Enter number: " NUMBER
				if [[ $NUMBER = "e" || $NUMBER = "exit" ]]; then
					GAME_EXIT=1
					SCRIPT_EXIT=1
				fi
			else
				GAME_EXIT=1
				WIN="LOSE"
			fi
		fi
		COUNT=$(($COUNT-1))
	done
}

menu()
{

	apdade_view
	case $CHOICE in
	  "s"|"start") game;;
	  "-r"|"--retries") read -p "Enter new retries: " RETRIES;;
	  "-u"|"--upper") read -p "Enter new upper border: " UPPER_BORDER;;
	  "-n"|"--number") read -p "Enter new number: " NUMBER;;
	  "e"|"exit") SCRIPT_EXIT=1;;
	esac
	apdade_view
}


check_param
while [ $SCRIPT_EXIT -eq 0 ]; do
	menu

	if [ $SCRIPT_EXIT -eq 0 ]; then
		read -p "Enter command: " CHOICE
	fi
done

echo "***** END Game: **********";


