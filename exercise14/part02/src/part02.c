#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/jiffies.h>
#include <linux/ktime.h>
#include <linux/slab.h>
#include <linux/vmalloc.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Yurii Osipchuk");
MODULE_VERSION("2.0");
MODULE_DESCRIPTION("Exercise14: Part02");
// 1 second = 1000000000 ns


void test_kmalloc(size_t count){
	ktime_t time_start = 0, time_end = 0, time_interval_a = 0, time_interval_f = 0;
	char *buffer = NULL;

	time_start = ktime_get_real();
	buffer = kmalloc(count, GFP_KERNEL);
	time_end = ktime_get_real();
	time_interval_a = time_end - time_start;

	if(buffer != NULL){
		buffer[0] = 1;
		buffer[count-1] = 1;
		time_start = ktime_get_real();
		kfree(buffer);
		time_end = ktime_get_real();
		time_interval_f = time_end - time_start;
	}

	printk(KERN_INFO"[kmalloc]: %6ld bytes | %6lld ns allocation | %6lld ns freeing\n", count, time_interval_a, time_interval_f);
}

void test_kzalloc(size_t count){
	ktime_t time_start = 0, time_end = 0, time_interval_a = 0, time_interval_f = 0;
	char *buffer = NULL;

	time_start = ktime_get_real();
	buffer = kzalloc(count, GFP_KERNEL);
	time_end = ktime_get_real();
	time_interval_a = time_end - time_start;

	if(buffer != NULL){
		buffer[0] = 1;
		buffer[count-1] = 1;
		time_start = ktime_get_real();
		kfree(buffer);
		time_end = ktime_get_real();
		time_interval_f = time_end - time_start;
	}

	printk(KERN_INFO"[kzalloc]: %6ld bytes | %6lld ns allocation | %6lld ns freeing\n", count, time_interval_a, time_interval_f);
}

void test_vmalloc(size_t count){
	ktime_t time_start = 0, time_end = 0, time_interval_a = 0, time_interval_f = 0;
	char *buffer = NULL;

	time_start = ktime_get_real();
	buffer = vmalloc(count);
	time_end = ktime_get_real();
	time_interval_a = time_end - time_start;

	if(buffer != NULL){
		buffer[0] = 1;
		buffer[count-1] = 1;
		time_start = ktime_get_real();
		vfree(buffer);
		time_end = ktime_get_real();
		time_interval_f = time_end - time_start;
	}

	printk(KERN_INFO"[vmalloc]: %6ld bytes | %6lld ns allocation | %6lld ns freeing\n", count, time_interval_a, time_interval_f);
}


static int __init mod_init(void)
{
	size_t i;
	printk(KERN_INFO "------------------------------------------------------------------\n");

	for (i = 1; i < 1000000; i *= 2)
	{
		test_kmalloc(i);
	}

	printk(KERN_INFO "------------------------------------------------------------------\n");

	for (i = 1; i < 1000000; i *= 2)
	{
		test_kzalloc(i);
	}

	printk(KERN_INFO "------------------------------------------------------------------\n");

	for (i = 1; i < 1000000; i *= 2)
	{
		test_vmalloc(i);
	}

	printk(KERN_INFO "------------------------------------------------------------------\n");


	return -1;
}

module_init(mod_init);
