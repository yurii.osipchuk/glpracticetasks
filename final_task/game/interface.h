#ifndef __INTERFACE_H__
#define __INTERFACE_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

char get_key_value(void);
int int_char(int ch);
int lcd_updata(void);
int lcd_clear(void);
int set_font(int value);
int set_line_cord(int x1, int y1, int x2, int y2);
int set_rectangle_cord(int x1, int y1, int x2, int y2);
int set_rgb_bg(int r, int g, int b);
int set_rgb_text(int r, int g, int b);
int set_xy_ch(int x, int y, int ch);
int set_xy_str(int x, int y, char *str);

#endif // __INTERFACE_H__