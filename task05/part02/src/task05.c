#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <linux/uaccess.h>
#include <linux/slab.h>
#include <linux/cdev.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Yurii Osipchuk");
MODULE_DESCRIPTION("Task05");
MODULE_VERSION("0.1");

#define PROC_DIRECTORY "example"
#define PROC_FILENAME "value"
#define BUFFER_SIZE 1024

static char buf_sys_msg[BUFFER_SIZE] = "0\n";

static char *proc_buffer;
static size_t proc_msg_length;
static size_t proc_msg_read_pos;

static struct proc_dir_entry *proc_dir;
static struct proc_dir_entry *proc_file;

static ssize_t example_read(struct file *file_p, char __user *buffer,
			    size_t length, loff_t *offset);
static ssize_t example_write(struct file *file_p, const char __user *buffer,
			     size_t length, loff_t *offset);
int str_to_int(char *str);

//********************************************************************//
static ssize_t sys_read(struct class *class, struct class_attribute *attr,
			char *buf)
{
	strcpy(buf, buf_sys_msg);
	return strlen(buf);
}

static ssize_t sys_write(struct class *class, struct class_attribute *attr,
			 const char *buf, size_t count)
{
	strncpy(buf_sys_msg, buf, count);
	buf_sys_msg[count] = '\0';
	return count;
}

#define CLASS_ATTR(_name, _mode, _show, _store)                                \
	struct class_attribute class_attr_##_name =                            \
		__ATTR(_name, _mode, _show, _store)
CLASS_ATTR(value, (S_IWUSR | S_IRUGO), &sys_read, &sys_write);

static struct class *sys_class;

//********************************************************************//

#ifdef QUEM
static struct proc_ops proc_fops = {
	.proc_read = example_read,
	.proc_write = example_write,
};
#endif

#ifdef NOTQUEM
static struct file_operations proc_fops = {
	.read = example_read,
	.write = example_write,
};
#endif

void converter(char *str)
{
	int UAH;
	int kurs;
	UAH = str_to_int(str);
	kurs = str_to_int(buf_sys_msg);
	sprintf(proc_buffer, "EURO = %d.%02d\n", UAH / kurs, UAH % kurs);
}

int str_to_int(char *str)
{
	int i, tmp, length, count = 0;
	length = strlen(str);
	for (i = 0; i < length - 1; ++i) {
		tmp = str[i] - 48;
		if (i > 0) {
			count = count * 10 + tmp;
		} else {
			count = tmp;
		}
	}
	return count;
}

static void uppercase_string(char *ch)
{
	int i;
	for (i = 0; ch[i] != '\0'; ++i) {
		if (ch[i] >= 97 && ch[i] <= 122) {
			ch[i] = ch[i] - 32;
		}
	}
}

void revers_string(char *ch)
{
	int i, j, k;
	char temp;

	for (i = 0, j = 0; ch[i] != 10; ++i) {
		if (ch[j] == ' ' && ch[i] != ' ') {
			j = i;
		}

		if ((ch[i] == ' ' || ch[i + 1] == 10) && ch[j] != ' ') {
			if (ch[i + 1] == 10) {
				k = i;
			} else {
				k = i - 1;
			}

			for (; j < k; ++j, k--) {
				temp = ch[j];
				ch[j] = ch[k];
				ch[k] = temp;
			}
			j = i;
		}
	}
}

static int create_buffer(void)
{
	proc_buffer = kmalloc(BUFFER_SIZE, GFP_KERNEL);
	if (NULL == proc_buffer)
		return -ENOMEM;
	proc_msg_length = 0;

	return 0;
}

static void cleanup_buffer(void)
{
	if (proc_buffer) {
		kfree(proc_buffer);
		proc_buffer = NULL;
	}
	proc_msg_length = 0;
}

static int create_proc_example(void)
{
	proc_dir = proc_mkdir(PROC_DIRECTORY, NULL);
	if (NULL == proc_dir)
		return -EFAULT;

	proc_file = proc_create(PROC_FILENAME, S_IFREG | S_IRUGO | S_IWUGO,
				proc_dir, &proc_fops);
	if (NULL == proc_file)
		return -EFAULT;

	return 0;
}

static void cleanup_proc_example(void)
{
	if (proc_file) {
		remove_proc_entry(PROC_FILENAME, proc_dir);
		proc_file = NULL;
	}
	if (proc_dir) {
		remove_proc_entry(PROC_DIRECTORY, NULL);
		proc_dir = NULL;
	}
}

static ssize_t example_read(struct file *file_p, char __user *buffer,
			    size_t length, loff_t *offset)
{
	size_t left;

	if (length > (proc_msg_length - proc_msg_read_pos))
		length = (proc_msg_length - proc_msg_read_pos);

	left = copy_to_user(buffer, &proc_buffer[proc_msg_read_pos], length);

	proc_msg_read_pos += length - left;

	return length - left;
}

static ssize_t example_write(struct file *file_p, const char __user *buffer,
			     size_t length, loff_t *offset)
{
	size_t msg_length;
	size_t left;

	if (length > BUFFER_SIZE) {
		msg_length = BUFFER_SIZE;
	} else
		msg_length = length;

	left = copy_from_user(proc_buffer, buffer, msg_length);
	printk(KERN_INFO "read message: UAH = %s", proc_buffer);
	printk(KERN_INFO "read message: conversion factors = %s", buf_sys_msg);

	converter(proc_buffer);
	msg_length = strlen(proc_buffer);
	printk(KERN_INFO "read message: %s", proc_buffer);

	proc_msg_length = msg_length - left;
	proc_msg_read_pos = 0;
	return length;
}

static int __init example_init(void)
{
	int err;

	err = create_buffer();
	if (err)
		goto error;

	err = create_proc_example();
	if (err)
		goto error;

	sys_class = class_create(THIS_MODULE, PROC_DIRECTORY);
	if (IS_ERR(sys_class))
		printk("bad class create\n");
	err = class_create_file(sys_class, &class_attr_value);
	if (err)
		goto error;

	printk(KERN_INFO "module task05 loaded\n");
	return 0;

error:
	printk(KERN_ERR "failed to load\n");
	cleanup_proc_example();
	cleanup_buffer();
	class_remove_file(sys_class, &class_attr_value);
	class_destroy(sys_class);
	return err;
}

static void __exit example_exit(void)
{
	cleanup_proc_example();
	cleanup_buffer();
	class_remove_file(sys_class, &class_attr_value);
	class_destroy(sys_class);
	printk(KERN_INFO "module task05 exited\n");
}

module_init(example_init);
module_exit(example_exit);
