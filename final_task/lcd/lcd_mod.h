#ifndef __LCD_MOD_H__
#define __LCD_MOD_H__

#include <linux/init.h>
#include <linux/module.h>
#include <linux/spi/spi.h>
#include <linux/delay.h>
#include <linux/gpio.h>
#include <linux/device.h>
#include "ili9341.h"
#include "fonts.h"

#define DATA_SIZE 95

inline void lcd_update_screen(void);
void lcd_draw_pixel(u16 x, u16 y, u16 color);
void lcd_fill_rectangle(u16 x, u16 y, u16 w, u16 h, u16 color);
void lcd_fill_screen(u16 color);
void lcd_put_char(u16 x, u16 y, char ch, FontDef font, u16 color, u16 bgcolor);
void lcd_init_ili9341(void);
void lcd_put_str(u16 x, u16 y, const char *str, FontDef font, u16 color,
		 u16 bgcolor);
void lcd_put_str_nline(u16 x, u16 y, const char *str, FontDef font, u16 color,
		       u16 bgcolor);
void lcd_draw_line(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2,
		   uint16_t color);
void exit_func(void);
int start_func(void);

#endif // __LCD_MOD_H__
