#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 80

int main(int argc, char *argv[])
{
	int i, j;
	int k = 0;
	int num = 0;
	char smb;
	char *code = malloc(MAX_LEN);
	char *str = malloc(MAX_LEN);

	scanf("%s", code);

	for (i = 0; i <= strlen(code); i++) {
		smb = code[i];
		if (smb >= 48 && smb <= 57) {
			num = (num * (10 * k)) + (smb - 48);
			k++;
		} else {
			for (j = 0; j < num; ++j) {
				printf("%c", code[i]);
			}
			k = 0;
			num = 0;
		}
	}

	free(code);

	return 0;
}
