# Author
Yurii Osipchuk
# Description
[Task #14](https://gl-khpi.gitlab.io/exercises/exercise14/).

## Memory management:

### Part01:
![Image01](res/part01.png)

### Part02:
![Image02](res/kmalloc.png)
![Image03](res/kzalloc.png)
![Image04](res/vmalloc.png)
