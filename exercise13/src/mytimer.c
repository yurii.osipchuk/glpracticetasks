#include <linux/init.h>
#include <linux/module.h>
#include <linux/timer.h>
#include <linux/version.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Yurii Osipchuk");
MODULE_DESCRIPTION("Exercise13");

struct timer_list my_timer;

/*
 * TIMER FUNCTION
 * */

#if (LINUX_VERSION_CODE < KERNEL_VERSION(4,15,0))
static void timer_function(unsigned long data){
    printk("Time up");
    // modify the timer for next time
    mod_timer(&my_timer, jiffies + HZ / 2);
}
#else
static void timer_function(struct timer_list *data){
    struct timer_list * p = data;
    printk("Time up");
    // modify the timer for next time
    mod_timer(p, p->expires + HZ / 2);
}
#endif

/*
 * INIT MODULE
 * */
int init_module(void)
{
	printk("Hello My Timer\n");
#if (LINUX_VERSION_CODE < KERNEL_VERSION(4,15,0))
    //  -- initialize the timer
    init_timer(&my_timer);
    // my_timer.expires = jiffies + HZ;
    my_timer.function = timer_function;
    my_timer.data = NULL;

    // -- TIMER START
    // add_timer(&my_timer);
#else
    timer_setup(&my_timer, timer_function, 0);
    mod_timer(&my_timer, jiffies + HZ);
    // my_timer.expires = jiffies + HZ;
    // my_timer.data = NULL;
#endif
    my_timer.expires = jiffies + HZ;
    // my_timer.data = NULL;

    // -- TIMER START
    // add_timer(&my_timer);
	printk("END: init_module() \n");
	return 0;
}

/*
 * CLEANUP MODULE
 * */
void cleanup_module(void)
{
	del_timer(&my_timer);
	printk("Goodbye\n");
}