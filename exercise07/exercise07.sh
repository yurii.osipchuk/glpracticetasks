#!/bin/bash

PIN_BUTTON=26
echo ${PIN_BUTTON} > /sys/class/gpio/export
echo in > /sys/class/gpio/gpio${PIN_BUTTON}/direction

START=1
DS3231_I2C=0x68
COUNT=0

ARG=0

BCD_TO_DECIMAL(){
    ARG=$(( ($1 / 16 * 10) + ($1 % 16) ))
}

DECIMAL_TO_BCD(){
    ARG=$(( ($1 / 10 * 16) + ($1 % 10) ))
}

DS3231_TIME(){
    VAR=0
    SECONDS=0    # 0x00
    MINUTES=0    # 0x01
    HOURS=0      # 0x02
    DAY=0        # 0x03
    DATE=0       # 0x04
    MONTH=0      # 0x05
    YEAR=0       # 0x06
    TEMP_SIGN=0  # 0x11
    TEMP_1=0     # 0x11
    TEMP_2=0     # 0x12

    VAR=$(i2cget -y 1 $DS3231_I2C 0x00)
    BCD_TO_DECIMAL $VAR
    SECONDS=$ARG

    VAR=$(i2cget -y 1 $DS3231_I2C 0x01)
    BCD_TO_DECIMAL $VAR
    MINUTES=$ARG

    VAR=$(i2cget -y 1 $DS3231_I2C 0x02)
    BCD_TO_DECIMAL $(($VAR & 0x0f))
    if [[ ($(($VAR & 0x10)) -eq 1) &&  ($HOURS < 10) ]];then
        HOURS=$(($HOURS + 10))
    fi
    if [[ ($(($VAR & 0x20)) -eq 1) &&  ($HOURS < 20) ]];then
        HOURS=$(($HOURS + 10))
    fi
    HOURS=$(($HOURS + $VAR))

    VAR=$(i2cget -y 1 $DS3231_I2C 0x03)
    DAY=$VAR

    VAR=$(i2cget -y 1 $DS3231_I2C 0x04)
    BCD_TO_DECIMAL $VAR
    DATE=$ARG

    VAR=$(i2cget -y 1 $DS3231_I2C 0x05)
    BCD_TO_DECIMAL $VAR
    MONTH=$ARG

    VAR=$(i2cget -y 1 $DS3231_I2C 0x06)
    BCD_TO_DECIMAL $VAR
    YEAR=$ARG

    VAR=$(i2cget -y 1 $DS3231_I2C 0x11)
    TEMP_SIGN=$(($VAR >> 7))
    TEMP_1=$(($VAR & 0x7f))
    VAR=$(i2cget -y 1 $DS3231_I2C 0x12)
    TEMP_2=$(( ($VAR & 0xc0) >> 6 ))

    if [ $TEMP_SIGN -eq 1 ];then
        TEMP_SIGN="-"
    else
        TEMP_SIGN="+"
    fi

    printf "%02d:%02d:%02d 20%02d-%02d-%02d TEMP = %c%02d.%02d\n" ${HOURS} ${MINUTES} ${SECONDS} ${YEAR} ${MONTH} ${DATE} ${TEMP_SIGN} ${TEMP_1} ${TEMP_2}
}

while [ $START -eq 1 ]; do
    COUNT=0
    BUTTON_DETECT=$(cat /sys/class/gpio/gpio${PIN_BUTTON}/value)
    if [[ $BUTTON_DETECT -eq 1 ]]; then
        DS3231_TIME
    fi
    while [[ $BUTTON_DETECT -eq 1 ]]; do
        BUTTON_DETECT=$(cat /sys/class/gpio/gpio${PIN_BUTTON}/value)
        sleep 0.1
        COUNT=$(($COUNT+1))
        if [[ $COUNT -eq 10 ]]; then
            START=0
            break
        fi
    done
done

echo ${PIN_BUTTON} > /sys/class/gpio/unexport

