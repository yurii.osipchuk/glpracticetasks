#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/uaccess.h>
#include <linux/slab.h>
#include <linux/jiffies.h>
#include <linux/types.h>
#include <linux/rtc.h>
#include <linux/ktime.h>
#include <linux/list.h>

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Yurii Osipchuk");
MODULE_DESCRIPTION("Task08");
MODULE_VERSION("1.0");

#define DIRECTORY "tassk08"

#define CLASS_ATTR(_name, _mode, _show, _store)                                \
	struct class_attribute class_attr_##_name =                            \
		__ATTR(_name, _mode, _show, _store)

static struct class *sys_class;
static size_t user_id = 0;

struct data {
	size_t id;
	size_t status;
	size_t sec;
	size_t time;
	char name[32];
	char msg[128];
	struct list_head list_t;
};

struct data *item = NULL;
struct list_head *iter = NULL, *iter_safe = NULL;
LIST_HEAD(list_t);

void parse_data(const char *str, char *name, char *msg, size_t *sec)
{
	size_t count = 0, temp = 0;
	*sec = 0;
	while (*str) {
		if (*str == '\n') {
			++str;
			continue;
		}

		if (*str == '|') {
			++count;
			++str;
			continue;
		}

		if (count == 0) {
			*name = *str;
			++name;
			*name = '\0';
			++str;
			continue;
		}

		if (count == 1) {
			*msg = *str;
			++msg;
			*msg = '\0';
			++str;
			continue;
		}

		if (count == 2) {
			if (*str >= 48 && *str <= 57) {
				temp = *str - 48;
				if (*sec != 0) {
					*sec = *sec * 10 + temp;
				} else {
					*sec = temp;
				}
			}
		}
		++str;
	}
}

static ssize_t value_read(struct class *class, struct class_attribute *attr,
			  char *buf)
{
	if (item == NULL && user_id == 0) {
		printk(KERN_INFO "value_read: [LIST] msg empty");
		sprintf(buf, "%s\n", "[LIST] msg empty");
	} else {
		printk(KERN_INFO "value_read");
		list_for_each_prev (iter, &list_t) {
			item = list_entry(iter, struct data, list_t);
			if (item->status == 1) {
				if (item->time > jiffies) {
					// printk( KERN_INFO "time: %ld\n", item->time);
					// printk( KERN_INFO "jiffies: %ld\n", jiffies);
					continue;
				} else {
					// printk( KERN_INFO "time: %ld\n", item->time);
					// printk( KERN_INFO "jiffies: %ld\n", jiffies);
					printk(KERN_INFO
					       "[LIST] id:%ld name: %s msg: %s sec: %ld\n",
					       item->id, item->name, item->msg,
					       item->sec);
					sprintf(buf,
						"[LIST] id:%ld name: %s msg: %s sec: %ld\n",
						item->id, item->name, item->msg,
						item->sec);
					item->status = 0;
					break;
				}
			}
		}
	}

	return strlen(buf);
}

static ssize_t value_write(struct class *class, struct class_attribute *attr,
			   const char *buf, size_t count)
{
	if (strlen(buf) > 6) {
		++user_id;
		item = kmalloc(sizeof(*item), GFP_KERNEL);
		item->id = user_id;
		item->status = 1;
		parse_data(buf, item->name, item->msg, &(item->sec));
		item->time = jiffies + (msecs_to_jiffies(item->sec * 1000));
		list_add(&(item->list_t), &list_t);
	}
	printk(KERN_INFO "value_write: buf strlen:%ld", strlen(buf));
	return count;
}

CLASS_ATTR(value, (S_IWUSR | S_IRUGO), &value_read, &value_write);

static int __init example_init(void)
{
	int err;

	sys_class = class_create(THIS_MODULE, DIRECTORY);
	if (IS_ERR(sys_class))
		printk("bad class create\n");
	err = class_create_file(sys_class, &class_attr_value);
	if (err)
		goto error;

	printk(KERN_NOTICE "loaded\n");
	return 0;

error:
	printk(KERN_ERR "failed to load\n");
	class_remove_file(sys_class, &class_attr_value);
	class_destroy(sys_class);
	return err;
}

static void __exit example_exit(void)
{
	if (item != NULL) {
		list_for_each_safe (iter, iter_safe, &list_t) {
			item = list_entry(iter, struct data, list_t);
			printk(KERN_INFO "[LIST] free id:%ld\n", item->id);
			list_del(iter);
			kfree(item);
		}
	}

	class_remove_file(sys_class, &class_attr_value);
	class_destroy(sys_class);
	printk(KERN_NOTICE "exited\n");
}

module_init(example_init);
module_exit(example_exit);
