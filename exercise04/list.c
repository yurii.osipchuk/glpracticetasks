#include "list.h"

void push_list(struct Node **head_ref, int key, char *name)
{
	struct Node *new_node = (struct Node *)malloc(sizeof(struct Node));

	new_node->key = key;
	new_node->name = name;
	new_node->result = 0;
	new_node->next = NULL;
	new_node->prev = NULL;

	if (*head_ref != NULL) {
		(*head_ref)->next = new_node;
		new_node->prev = *head_ref;
		*head_ref = new_node;
	} else {
		*head_ref = new_node;
	}
}

struct Node *find_start(struct Node *head_ref)
{
	if (head_ref == NULL) {
		return NULL;
	}

	struct Node *link = head_ref;
	while (link->prev != NULL) {
		link = link->prev;
	}
	return link;
}

struct Node *find_end(struct Node *head_ref)
{
	if (head_ref == NULL) {
		return NULL;
	}

	struct Node *link = head_ref;
	while (link->next != NULL) {
		link = link->next;
	}
	return link;
}

void print_list(struct Node *head_ref)
{
	struct Node *link = find_start(head_ref);
	while (link != NULL) {
		printf("[%d] player = %s : result = %d\n", link->key,
		       link->name, link->result);
		link = link->next;
	}
}

void set_list(struct Node *head_ref, int key, int result)
{
	struct Node *link = find_start(head_ref);

	if (link != NULL) {
		while (link != NULL) {
			if (link->key == key) {
				link->result += result;
			}
			link = link->next;
		}
	}
}

int get_list_result(struct Node *head_ref, int key)
{
	struct Node *link = find_start(head_ref);

	if (link != NULL) {
		while (link != NULL) {
			if (link->key == key) {
				return link->result;
			}
			link = link->next;
		}
	}
	return -1;
}

char *get_list_name(struct Node *head_ref, int key)
{
	struct Node *link = find_start(head_ref);

	if (link != NULL) {
		while (link != NULL) {
			if (link->key == key) {
				return link->name;
			}
			link = link->next;
		}
	}
	return "";
}

void close_list(struct Node *head_ref)
{
	struct Node *link = find_start(head_ref);
	struct Node *temp = NULL;

	if (link != NULL) {
		while (link != NULL) {
			temp = link;
			link = link->next;
			free(temp);
		}
	}
}
