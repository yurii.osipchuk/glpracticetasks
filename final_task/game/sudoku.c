#include "sudoku.h"

int x = CORD_X;
int y = CORD_Y;

int example1[81] = {
	0, 1, 9, 0, 0, 2, 0, 0, 0,
	4, 7, 0, 6, 9, 0, 0, 0, 1,
	0, 0, 0, 4, 0, 0, 0, 9, 0,
	8, 9, 4, 5, 0, 7, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 2, 0, 1, 9, 5, 8,
	0, 5, 0, 0, 0, 6, 0, 0, 0,
	6, 0, 0, 0, 2, 8, 0, 7, 9,
	0, 0, 0, 1, 0, 0, 8, 6, 0
};

int example2[81] = {
	0, 0, 5, 2, 8, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 4, 1, 0, 0,
	0, 0, 9, 0, 0, 0, 4, 0, 3,
	9, 0, 0, 7, 0, 0, 0, 6, 0,
	0, 8, 0, 0, 1, 0, 0, 4, 0,
	0, 5, 0, 0, 0, 9, 0, 0, 1,
	4, 0, 6, 0, 0, 0, 2, 0, 0,
	0, 0, 7, 4, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 2, 5, 6, 0, 0
};

void init_sudoku_table(Element_sudoku *Table_sudoku)
{
	int i, j;
	int border_x = 2;
	int border_y = 2;
	int count = 9;
	int font_height = 18;

	for (i = 0; i < count; ++i) {
		border_x = 2;

		for (j = 0; j < count; ++j) {
			Table_sudoku[i * count + j].width =
				x + border_x + font_height * j;
			Table_sudoku[i * count + j].height =
				y + border_y + font_height * i;

			++border_x;
			if (border_x == 5 || border_x == 9) {
				++border_x;
			}
		}
		++border_y;
		if (border_y == 5 || border_y == 9) {
			++border_y;
		}
	}
}

void init_sudoku_game(Element_sudoku *Table_sudoku, int *example)
{
	int i;
	for (i = 0; i < ELEMENTS_COUNT; ++i) {
		Table_sudoku[i].first = example[i];
		Table_sudoku[i].second = example[i];
		if (0 == example[i]) {
			Table_sudoku[i].check = 1;
		} else {
			Table_sudoku[i].check = 0;
		}
	}
}

void print_table(Element_sudoku *Table_sudoku)
{
	int i;
	for (i = 0; i < ROW * COL; ++i) {
		if (i != 0 && i % ROW == 0) {
			printf("\n");
		}
		printf("%d ", Table_sudoku[i].first);
	}
	printf("\n");
}

void print_solve_table(Element_sudoku *Table_sudoku)
{
	int i;
	for (i = 0; i < ROW * COL; ++i) {
		if (i != 0 && i % ROW == 0) {
			printf("\n");
		}
		printf("%d ", Table_sudoku[i].second);
	}
	printf("\n");
}

void find_next_empty(Element_sudoku *Table_sudoku, int *row, int *col)
{
	int i;
	for (i = 0; i < ROW * COL; ++i) {
		if (Table_sudoku[i].second == 0) {
			*row = i / 9;
			*col = i % 9;
			return;
		}
	}
	*row = -1;
	*col = -1;
}

int is_valid(Element_sudoku *Table_sudoku, int guess, int row, int col)
{
	int i, j;
	int row_vals = 0;
	int col_vals = 0;
	int row_start = 0;
	int col_start = 0;

	for (i = 0; i < 9; ++i) {
		row_vals = Table_sudoku[row * 9 + i].second;
		if (row_vals == guess) {
			return 0;
		}
	}

	for (i = 0; i < 9; ++i) {
		col_vals = Table_sudoku[i * 9 + col].second;
		if (col_vals == guess) {
			return 0;
		}
	}

	row_start = (row / 3) * 3;
	col_start = (col / 3) * 3;

	for (i = row_start; i < row_start + 3; ++i) {
		for (j = col_start; j < col_start + 3; ++j) {
			if (guess == Table_sudoku[i * 9 + j].second) {
				return 0;
			}
		}
	}

	return 1;
}

int solve_sudoku(Element_sudoku *Table_sudoku)
{
	int i;
	int row, col;
	find_next_empty(Table_sudoku, &row, &col);
	if (row == -1) {
		return 1;
	}

	for (i = 1; i < 10; ++i) {
		if (is_valid(Table_sudoku, i, row, col)) {
			Table_sudoku[row * 9 + col].second = i;
			if (solve_sudoku(Table_sudoku)) {
				return 1;
			}
		}
		Table_sudoku[row * 9 + col].second = 0;
	}
	return 0;
}

int *sudoku_example(int num)
{
	if (num % 2 == 1) {
		return example1;
	} else if (num % 2 == 0) {
		return example2;
	}
}

int check_win(Element_sudoku *Table_sudoku)
{
	int i;
	for (i = 0; i < ELEMENTS_COUNT; ++i) {
		if (Table_sudoku[i].first != Table_sudoku[i].second) {
			return 0;
		}
	}
	return 1;
}
