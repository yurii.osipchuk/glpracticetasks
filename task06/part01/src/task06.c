//--------------------------------------------------//
#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/uaccess.h>
#include <linux/version.h>

//--------------------------------------------------//
#define DEVICE_FIRST 0
#define DEVICE_COUNT 1
#define MODNAME "my_dyndev_mod"
#define PROC_DIRECTORY "task06proc"
#define PROC_FILENAME "buffer_info"
#define DEVNAME "msg0"

#define CLASS_ATTR(_name, _mode, _show, _store)      \
	struct class_attribute class_attr_##_name =      \
		__ATTR(_name, _mode, _show, _store)

//--------------------------------------------------//
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Yurii Osipchuk");
MODULE_DESCRIPTION("Task06");
MODULE_VERSION("6.1");
//--------------------------------------------------//
static int buffer_size = 1024;
static const int buffer_size_max = 1024;
static const size_t messages_count = 0;
// static char sysfs_user_bg_color[64] = "empty\n";
// static char sysfs_user_text_color[64] = "empty\n";
// static char sysfs_user_font[64] = "empty\n";
static char sysfs_user_name[64] = "empty\n";
static char *dev_buffer = NULL;

static int major = 0;

static struct cdev hcdev;
static struct class *devclass;
static struct proc_dir_entry *proc_dir;
static struct proc_dir_entry *proc_file;

//--------------------------------------------------//
module_param(major, int, S_IRUGO);
module_param(buffer_size, int, S_IRUGO);

//--------------------------------------------------//
static ssize_t dev_read(struct file *file, char *user_buffer, size_t count,
			loff_t *offset);
static ssize_t dev_write(struct file *file, const char __user *user_buffer,
			 size_t count, loff_t *offset);
static int create_proc(void);
static void cleanup_proc(void);
static ssize_t proc_fs_read(struct file *file, char *buf, size_t count,
			    loff_t *ppos);
// value
static ssize_t sys_read_value(struct class *class, struct class_attribute *attr,
			  char *buf);
static ssize_t sys_write_value(struct class *class, struct class_attribute *attr,
			   const char *buf, size_t count);
// user
static ssize_t sys_read_user(struct class *class, struct class_attribute *attr,
			  char *buf);
static ssize_t sys_write_user(struct class *class, struct class_attribute *attr,
			   const char *buf, size_t count);
static int create_buffer(void);
static void free_buffer(void);
static void clear_buffer(void);
int str_to_int(const char *str);
size_t count_messages(const char *str);
void print_str(char *str, size_t count);


//--------------------------------------------------//
// cdev
static const struct file_operations dev_fops = {
	.owner = THIS_MODULE,
	.read = dev_read,
	.write = dev_write,
};

#ifdef NOTQUEM
static const struct file_operations proc_fops = { .read = proc_fs_read };
#endif

#ifdef QUEM
static struct proc_ops proc_fops = {
	.proc_read = proc_fs_read,
};
#endif

//--------------------------------------------------//
// Other helps function
int str_to_int(const char *str)
{
	int i, tmp, length, count = 0;
	length = strlen(str);
	for (i = 0; i < length - 1; ++i) {
		tmp = str[i] - 48;
		if (i > 0) {
			count = count * 10 + tmp;
		} else {
			count = tmp;
		}
	}
	return count;
}

void print_str(char *str, size_t count)
{
	size_t i;
	printk(KERN_INFO "print_str:\n");
	for (i = 0; i < count; ++i) {
		printk(KERN_INFO "%02d", str[i]);
	}
	printk(KERN_INFO "\n");
}

size_t count_messages(const char *str)
{
	size_t count = 0;
	size_t i;
	size_t bytes = strlen(str);
	char ch = '\n'; // 10

	for (i = 0; i < bytes; ++i) {
		if (str[i] == ch && i != 0) {
			++count;
		}
	}

	return count;
}

//--------------------------------------------------//
// function for dev_buffer
static int create_buffer(void)
{
	char *temp_bufer = NULL;
	size_t count;
	if (buffer_size > buffer_size_max) {
		buffer_size = buffer_size_max;
	}

	temp_bufer = kmalloc(buffer_size, GFP_KERNEL);
	if (NULL == temp_bufer) {
		return -ENOMEM;
	}
	memset(temp_bufer, '\0', buffer_size);

	if (dev_buffer != NULL) {
		count = strlen(dev_buffer);
		count = count > buffer_size ? buffer_size : count;
		strncpy(temp_bufer, dev_buffer, count - 1);
		temp_bufer[count] = '\0';
		kfree(dev_buffer);
	}
	dev_buffer = temp_bufer;
	temp_bufer = NULL;
	printk(KERN_INFO "create_buffer(): buffer_size = %d\n", buffer_size);
	return 0;
}

static void free_buffer(void)
{
	if (dev_buffer) {
		kfree(dev_buffer);
		dev_buffer = NULL;
		printk(KERN_INFO "free_buffer(): ok\n");
	}
}

static void clear_buffer(void)
{
	memset(dev_buffer, '\0', buffer_size);
	printk(KERN_INFO "clear_buffer(): ok\n");
}

//--------------------------------------------------//
//--------------------------------------------------//
// CDEV
static ssize_t dev_read(struct file *file, char *user_buffer, size_t count,
			loff_t *offset)
{
	int retval = 0;
	size_t bytes;

	bytes = strlen(dev_buffer) - (*offset);

	printk(KERN_INFO "=== to user read buffer len : %ld\n",
	       (long)strlen(dev_buffer));
	printk(KERN_INFO "=== to user offset: %ld\n", (long)*offset);

	if (bytes) {
		retval = copy_to_user(user_buffer, dev_buffer, bytes);
		if (!retval) {
			*offset = bytes;
			printk(KERN_INFO "=== to user offset after: %ld\n",
			       (long)*offset);
			printk(KERN_INFO "=== to user read retval true: %d\n",
			       retval);
		} else {
			printk(KERN_INFO "=== to user read retval false: %d\n",
			       retval);
			return retval;
		}
	}
	return bytes;
}

static ssize_t dev_write(struct file *file, const char __user *user_buffer,
			 size_t count, loff_t *offset)
{
	int retval = 0;
	size_t bytes = 0;
	size_t i;
	char ch = 0;
	size_t len = strlen(dev_buffer);

	printk(KERN_INFO "=== from user buffer len : %ld\n", (long)len);
	printk(KERN_INFO "=== bytes from user : %ld\n", (long)count);

	for (i = 0; i < count; ++i) {
		retval = copy_from_user(&ch, user_buffer + i, 1);
		if ((buffer_size - 1 - len) > 0) {
			strcat(dev_buffer, &ch);
			++len;
			++bytes;
		}
	}
	if (retval) {
		return retval;
	}
	printk(KERN_INFO "=== bytes from user read : %ld\n", (long)bytes);
	return count;
}

//--------------------------------------------------//
// PROCFS
static int create_proc(void)
{
	proc_dir = proc_mkdir(PROC_DIRECTORY, NULL);
	if (NULL == proc_dir) {
		printk(KERN_ERR "=== Can not create procfs directory\n");
		return -EFAULT;
	}

	proc_file = proc_create(PROC_FILENAME, S_IFREG | S_IRUGO | S_IWUGO,
				proc_dir, &proc_fops);
	if (NULL == proc_file) {
		printk(KERN_ERR "=== Can not create procfs file\n");
		return -EFAULT;
	}
	return 0;
}

static void cleanup_proc(void)
{
	if (proc_file) {
		remove_proc_entry(PROC_FILENAME, proc_dir);
		printk(KERN_INFO "=== Cleanup procfs file\n");
		proc_file = NULL;
	}
	if (proc_dir) {
		remove_proc_entry(PROC_DIRECTORY, NULL);
		printk(KERN_INFO "=== Cleanup procfs directory\n");
		proc_dir = NULL;
	}
}

static ssize_t proc_fs_read(struct file *file, char *buf, size_t count,
			    loff_t *ppos)
{
	int len;
	char proc_buff[64];
	sprintf(proc_buff,
		"buffer size: %d\ncount messages: %ld\nfree space: %ld\n",
		buffer_size, (long)count_messages(dev_buffer),
		(long)(buffer_size - strlen(dev_buffer)));
	len = strlen(proc_buff);
	if (count < len)
		return -EINVAL;
	if (*ppos != 0) {
		printk(KERN_INFO "=== procfs read return : 0\n"); // EOF
		return 0;
	}
	printk(KERN_INFO "%s", proc_buff);
	if (copy_to_user(buf, proc_buff, len))
		return -EINVAL;
	*ppos = len;
	return len;
}

//--------------------------------------------------//
//--------------------------------------------------//
// SYSFS
// to userspace
static ssize_t sys_read_value(struct class *class, struct class_attribute *attr,
			  char *buf)
{
	sprintf(buf, "%d\n", buffer_size);
	printk(KERN_INFO "sysfs read value: %d\n", buffer_size);
	return strlen(buf);
}

// from usercpace
static ssize_t sys_write_value(struct class *class, struct class_attribute *attr,
			   const char *buf, size_t count)
{
	buffer_size = str_to_int(buf);
	create_buffer();
	printk(KERN_INFO "sysfs write value: %d\n", buffer_size);
	return count;
}

CLASS_ATTR(value, (S_IWUSR | S_IRUGO), &sys_read_value, &sys_write_value);

// to userspace
static ssize_t sys_read_user(struct class *class, struct class_attribute *attr,
			  char *buf)
{
	strcpy(buf, sysfs_user_name);
	printk(KERN_INFO "sysfs read user: %s", sysfs_user_name);
	return strlen(buf);
}

// from usercpace
static ssize_t sys_write_user(struct class *class, struct class_attribute *attr,
			   const char *buf, size_t count)
{
	memset(sysfs_user_name, '\0', strlen(sysfs_user_name));
	strncpy(sysfs_user_name, buf, count);
	printk(KERN_INFO "sysfs write user: %s", sysfs_user_name);
	return count;
}

CLASS_ATTR(user, (S_IWUSR | S_IRUGO), &sys_read_user, &sys_write_user);

//--------------------------------------------------//
static int __init dev_init(void)
{
	int ret, i;
	dev_t dev;

	if (major != 0) {
		// статичный major
		dev = MKDEV(major, DEVICE_FIRST);
		ret = register_chrdev_region(dev, DEVICE_COUNT, MODNAME);
		printk(KERN_INFO "register_chrdev_region: %d", ret);
	} else {
		// динамичный major
		ret = alloc_chrdev_region(&dev, DEVICE_FIRST, DEVICE_COUNT,
					  MODNAME);
		printk(KERN_INFO "alloc_chrdev_region: %d", ret);
		major = MAJOR(dev); // не забыть зафиксировать!
		printk(KERN_INFO "major: %d", major);
	}

	if (ret < 0) {
		printk(KERN_ERR "=== Can not register char device region\n");
		goto err;
	}

	cdev_init(&hcdev, &dev_fops);
	hcdev.owner = THIS_MODULE;
	ret = cdev_add(&hcdev, dev, DEVICE_COUNT);
	if (ret < 0) {
		unregister_chrdev_region(MKDEV(major, DEVICE_FIRST),
					 DEVICE_COUNT);
		printk(KERN_ERR "=== Can not add char device\n");
		goto err;
	}

	// struct class*
	devclass = class_create(THIS_MODULE, "task06");
	for (i = 0; i < DEVICE_COUNT; i++) {
		dev = MKDEV(major, DEVICE_FIRST + i);
#if LINUX_VERSION_CODE <= KERNEL_VERSION(2, 6, 26)
		device_create(devclass, NULL, dev, "%s%d", DEVNAME, i + 1);
#else
		device_create(devclass, NULL, dev, NULL, "%s%d", DEVNAME,
			      i + 1);
#endif
	}

	printk(KERN_INFO "======== module installed %d:[%d-%d] ===========\n",
	       MAJOR(dev), DEVICE_FIRST, MINOR(dev));

	ret = class_create_file(devclass, &class_attr_value); // sys_fs value
	if (ret) {
		printk(KERN_ERR "=== Can not class create file value\n");
		goto err;
	}

	ret = class_create_file(devclass, &class_attr_user); // sys_fs user
	if (ret) {
		printk(KERN_ERR "=== Can not class create file user\n");
		goto err;
	}

	ret = create_proc(); // proc_fs value
	if (ret) {
		printk(KERN_ERR "=== Can not create procfs\n");
		goto err;
	}

	create_buffer();

	return 0;

err:
	cleanup_proc();
	free_buffer();
	return ret;
}
//--------------------------------------------------//
static void __exit dev_exit(void)
{
	dev_t dev;
	int i;

	cleanup_proc(); // proc_fs
	class_remove_file(devclass, &class_attr_value); // sys_fs value
	class_remove_file(devclass, &class_attr_user); // sys_fs user

	for (i = 0; i < DEVICE_COUNT; i++) {
		dev = MKDEV(major, DEVICE_FIRST + i);
		device_destroy(devclass, dev);
	}
	class_destroy(devclass);
	cdev_del(&hcdev);
	unregister_chrdev_region(MKDEV(major, DEVICE_FIRST), DEVICE_COUNT);

	free_buffer();

	printk(KERN_INFO "=============== module removed ================\n");
}
//--------------------------------------------------//
module_init(dev_init);
module_exit(dev_exit);
//--------------------------------------------------//
