
### Task02: write a Guess the Number game using the Bash script
- Subtask: create a random number generator script

* ea35162 (HEAD -> dev, origin/master, origin/HEAD, master) info: updated readme.md, add description Subtask
* bdd43b4 info: updated readme.md, add description Task02
* 49766ca repo: created readme.md

Subtask completed

Subtask: Extract Function refactoring

* c410600 (HEAD -> dev) info: updated task02.sh, implemented random number generation
* 7665d36 (origin/dev) Merge branch 'master' into dev
* 2527318 info: updated readme.md
* c9d2d38 info: add bash script task02.sh
* ea35162 info: updated readme.md, add description Subtask
* bdd43b4 info: updated readme.md, add description Task02
* 49766ca repo: created readme.md

Subtask completed

- implement comparison of randomly generated number X with number Y
