#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 80

int main(int argc, char *argv[])
{
	int i;
	int cnt;
	char smb;
	char *code = malloc(MAX_LEN);
	char *encode = malloc(MAX_LEN);
	char *str = malloc(MAX_LEN);

	scanf("%s", code);
	strcpy(encode, "");

	smb = code[0];
	cnt = 0;

	for (i = 0; i <= strlen(code); i++) {
		if (code[i] == smb) {
			cnt++;
		} else {
			sprintf(str, "%d", cnt);
			strcat(encode, str);
			sprintf(str, "%c", smb);
			strcat(encode, str);
			smb = code[i];
			cnt = 1;
		}
	}

	printf("%s", encode);

	free(code);
	free(encode);
	free(str);

	return 0;
}
