#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/uaccess.h>
#include <linux/cdev.h>
#include <linux/jiffies.h>
#include <linux/types.h>
#include <linux/rtc.h>
#include <linux/ktime.h>

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Yurii Osipchuk");
MODULE_DESCRIPTION("Task07");
MODULE_VERSION("3.0");

#define DIRECTORY "tassk07"

#define CLASS_ATTR(_name, _mode, _show, _store)                                \
	struct class_attribute class_attr_##_name =                            \
		__ATTR(_name, _mode, _show, _store)

static struct class *sys_class;
static u32 time_start;
static u32 set_format = 0;
static struct timespec64 ts;
static struct rtc_time tm;

static ssize_t value_read(struct class *class, struct class_attribute *attr,
			  char *buf)
{
	u32 time_end, temp;
	u32 h, m, s;
	u32 hh, mm, ss;

	memset(&ts, 0, sizeof(ts));
	memset(&tm, 0, sizeof(tm));
	ktime_get_real_ts64(&ts);
	ts.tv_sec += 3600 * 3;

	rtc_time64_to_tm(ts.tv_sec, &tm);

	time_end = jiffies;
	if (time_start == 0 || time_end < time_start) {
		temp = 0;
	} else {
		temp = time_end - time_start;
	}
	time_start = jiffies;
	h = ((temp / HZ) / 3600) % 24;
	m = ((temp / HZ) / 60) % 60;
	s = (temp / HZ) % 60;

	hh = ((time_end / HZ) / 3600) % 24;
	mm = ((time_end / HZ) / 60) % 60;
	ss = (time_end / HZ) % 60;

	if (set_format == 0) {
		sprintf(buf,
			"seconds = %d\nabsolute time seconds = %lld\nuptime seconds = %d\n",
			temp / HZ, ts.tv_sec, time_end / HZ);
		printk(KERN_INFO "seconds = %d\n", temp / HZ);
		printk(KERN_INFO "absolute time seconds = %lld\n", ts.tv_sec);
		printk(KERN_INFO "uptime seconds = %d\n", time_end / HZ);

	} else {
		sprintf(buf,
			"%02d:%02d:%02d\nabsolute time: %02d/%02d/%04d - %02d:%02d:%02d\nuptime: %02d:%02d:%02d\n",
			h, m, s, tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900,
			tm.tm_hour, tm.tm_min, tm.tm_sec, hh, mm, ss);
		printk(KERN_INFO "%02d:%02d:%02d\n", h, m, s);
		printk(KERN_INFO
		       "absolute time: %02d/%02d/%04d - %02d:%02d:%02d\n",
		       tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900, tm.tm_hour,
		       tm.tm_min, tm.tm_sec);
		printk(KERN_INFO "uptime: %02d:%02d:%02d\n", hh, mm, ss);
	}

	return strlen(buf);
}

static ssize_t value_write(struct class *class, struct class_attribute *attr,
			   const char *buf, size_t count)
{
	if (buf[0] == '1') {
		set_format = 1;
	}
	return count;
}

CLASS_ATTR(value, (S_IWUSR | S_IRUGO), &value_read, &value_write);

static int __init example_init(void)
{
	int err;
	time_start = 0;

	sys_class = class_create(THIS_MODULE, DIRECTORY);
	if (IS_ERR(sys_class))
		printk("bad class create\n");
	err = class_create_file(sys_class, &class_attr_value);
	if (err)
		goto error;

	printk(KERN_NOTICE "loaded\n");
	return 0;

error:
	printk(KERN_ERR "failed to load\n");
	class_remove_file(sys_class, &class_attr_value);
	class_destroy(sys_class);
	return err;
}

static void __exit example_exit(void)
{
	class_remove_file(sys_class, &class_attr_value);
	class_destroy(sys_class);
	printk(KERN_NOTICE "exited\n");
}

module_init(example_init);
module_exit(example_exit);
