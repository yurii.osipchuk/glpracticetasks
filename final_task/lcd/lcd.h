#ifndef __LCD_H__
#define __LCD_H__

#include <linux/module.h>
#include <linux/uaccess.h>
#include <linux/device.h>
#include <linux/types.h>

#include "lcd_mod.h"

//--------------------------------------------------//
#define COLOR_BLACK 0x0000
#define COLOR_BLUE 0x001F
#define COLOR_RED 0xF800
#define COLOR_GREEN 0x07E0
#define COLOR_CYAN 0x07FF
#define COLOR_MAGENTA 0xF81F
#define COLOR_YELLOW 0xFFE0
#define COLOR_WHITE 0xFFFF
//--------------------------------------------------//
// sysfs_user_font:
// 0 = Font_7x10;
// 1 = Font_11x18;
// 2 = Font_16x26;
static int sysfs_user_font = 1;
static uint32_t sysfs_user_bg_color = COLOR_WHITE;
static uint32_t sysfs_user_text_color = COLOR_BLACK;

static struct class *lcdclass;

//--------------------------------------------------//
FontDef int_font(int num);
uint32_t color_color565(const char *str);
int str_to_int(const char *str);
void lcd_xy_ch(const char *str, int *x, int *y, char *ch);
void lcd_xy_str(const char *str, int *x, int *y, char *str_out);
void lcd_line_x1y1_x2y2(const char *str, int *x1, int *y1, int *x2, int *y2);

//--------------------------------------------------//
#define CLASS_NAME "lcd"

#define CLASS_ATTR(_name, _mode, _show, _store)                                \
	struct class_attribute class_attr_##_name =                            \
		__ATTR(_name, _mode, _show, _store)

//--------------------------------------------------//

// to userspace
static ssize_t sys_read_value(struct class *class, struct class_attribute *attr,
			      char *buf)
{
	return strlen(buf);
}

// from userspace
static ssize_t sys_write_value(struct class *class,
			       struct class_attribute *attr, const char *buf,
			       size_t count)
{
	int x = 0, y = 0;
	char ch;

	lcd_xy_ch(buf, &x, &y, &ch);
	lcd_put_char(x, y, ch, int_font(sysfs_user_font), sysfs_user_text_color,
		     sysfs_user_bg_color);

	printk(KERN_INFO "sysfs from userspace: %s", buf);
	return count;
}

CLASS_ATTR(value, (S_IWUSR | S_IRUGO), &sys_read_value, &sys_write_value);

// to userspace
static ssize_t sys_read_str(struct class *class, struct class_attribute *attr,
			    char *buf)
{
	return strlen(buf);
}

// from userspace
static ssize_t sys_write_str(struct class *class, struct class_attribute *attr,
			     const char *buf, size_t count)
{
	int x = 0, y = 0;
	char ch[64];

	lcd_xy_str(buf, &x, &y, ch);
	lcd_put_str(x, y, ch, int_font(sysfs_user_font), sysfs_user_text_color,
		    sysfs_user_bg_color);

	printk(KERN_INFO "sysfs from userspace: %s", buf);
	return count;
}

CLASS_ATTR(str, (S_IWUSR | S_IRUGO), &sys_read_str, &sys_write_str);

// to userspace
static ssize_t sys_read_text_color(struct class *class,
				   struct class_attribute *attr, char *buf)
{
	sprintf(buf, "text color: 0x%04x\n", sysfs_user_text_color);
	printk(KERN_INFO "read text color: 0x%04x\n", sysfs_user_text_color);
	return strlen(buf);
}

// from usercpace
static ssize_t sys_write_text_color(struct class *class,
				    struct class_attribute *attr,
				    const char *buf, size_t count)
{
	sysfs_user_text_color = color_color565(buf);
	printk(KERN_INFO "write text color: %s", buf);
	return count;
}

CLASS_ATTR(text_color, (S_IWUSR | S_IRUGO), &sys_read_text_color,
	   &sys_write_text_color);

// to userspace
static ssize_t sys_read_bg_color(struct class *class,
				 struct class_attribute *attr, char *buf)
{
	sprintf(buf, "bg color: 0x%04x\n", sysfs_user_bg_color);
	printk(KERN_INFO "read bg color: 0x%04x\n", sysfs_user_bg_color);
	return strlen(buf);
}

// from usercpace
static ssize_t sys_write_bg_color(struct class *class,
				  struct class_attribute *attr, const char *buf,
				  size_t count)
{
	sysfs_user_bg_color = color_color565(buf);
	printk(KERN_INFO "write bg color: %s", buf);
	return count;
}

CLASS_ATTR(bg_color, (S_IWUSR | S_IRUGO), &sys_read_bg_color,
	   &sys_write_bg_color);

// to userspace
static ssize_t sys_read_font(struct class *class, struct class_attribute *attr,
			     char *buf)
{
	sprintf(buf, "text font: %d\n", sysfs_user_font);
	printk(KERN_INFO "read text font: %d\n", sysfs_user_font);
	return strlen(buf);
}

// from usercpace
static ssize_t sys_write_font(struct class *class, struct class_attribute *attr,
			      const char *buf, size_t count)
{
	int font_num = str_to_int(buf);
	if (font_num >= 0 && font_num < 3) {
		sysfs_user_font = font_num;
	} else {
		sysfs_user_font = 1;
	}

	printk(KERN_INFO "write text font: %s", buf);
	return count;
}

CLASS_ATTR(font, (S_IWUSR | S_IRUGO), &sys_read_font, &sys_write_font);

// to userspace
static ssize_t sys_read_line(struct class *class, struct class_attribute *attr,
			     char *buf)
{
	return strlen(buf);
}

// from usercpace
static ssize_t sys_write_line(struct class *class, struct class_attribute *attr,
			      const char *buf, size_t count)
{
	int x1 = 0, x2 = 0, y1 = 0, y2 = 0;

	lcd_line_x1y1_x2y2(buf, &x1, &y1, &x2, &y2);

	lcd_draw_line(x1, y1, x2, y2, sysfs_user_text_color);

	printk(KERN_INFO "x1 = %d, y1 = %d, x2 = %d, y2 = %d\n", x1, y1, x2,
	       y2);
	printk(KERN_INFO "write text line: %s", buf);
	return count;
}

CLASS_ATTR(line, (S_IWUSR | S_IRUGO), &sys_read_line, &sys_write_line);

// to userspace
static ssize_t sys_read_updata(struct class *class,
			       struct class_attribute *attr, char *buf)
{
	lcd_update_screen();
	printk(KERN_INFO "read updata");
	return strlen(buf);
}

// from usercpace
static ssize_t sys_write_updata(struct class *class,
				struct class_attribute *attr, const char *buf,
				size_t count)
{
	return count;
}

CLASS_ATTR(updata, (S_IWUSR | S_IRUGO), &sys_read_updata, &sys_write_updata);

// to userspace
static ssize_t sys_read_clear(struct class *class, struct class_attribute *attr,
			      char *buf)
{
	lcd_fill_screen(sysfs_user_bg_color);
	printk(KERN_INFO "read updata");
	return strlen(buf);
}

// from usercpace
static ssize_t sys_write_clear(struct class *class,
			       struct class_attribute *attr, const char *buf,
			       size_t count)
{
	return count;
}

CLASS_ATTR(clear, (S_IWUSR | S_IRUGO), &sys_read_clear, &sys_write_clear);

// to userspace
static ssize_t sys_read_rectangle(struct class *class,
				  struct class_attribute *attr, char *buf)
{
	return strlen(buf);
}

// from usercpace
static ssize_t sys_write_rectangle(struct class *class,
				   struct class_attribute *attr,
				   const char *buf, size_t count)
{
	int x1 = 0, x2 = 0, y1 = 0, y2 = 0;

	lcd_line_x1y1_x2y2(buf, &x1, &y1, &x2, &y2);

	printk(KERN_INFO "read updata rectangle");
	printk(KERN_INFO "x1 = %d, y1 = %d, x2 = %d, y2 = %d\n", x1, y1, x2,
	       y2);
	printk(KERN_INFO "write text line: %s", buf);

	lcd_fill_rectangle(x1, y1, x2, y2, sysfs_user_bg_color);
	return count;
}

CLASS_ATTR(rectangle, (S_IWUSR | S_IRUGO), &sys_read_rectangle,
	   &sys_write_rectangle);

#endif // __LCD_H__