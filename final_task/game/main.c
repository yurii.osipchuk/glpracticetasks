#include "sudoku.h"
#include "interface.h"

int height = 18;
int width = 11;

void show_table(int x, int y)
{
	int x1 = x;
	int y1 = y;
	int x2 = x;
	int y2 = y;
	int table_width = 1 * 4 + 1 * 10 + height * 9 - 2;
	int table_height = 1 * 4 + 1 * 10 + height * 9 - 2;

	set_line_cord(x1, y1, x2 + table_width, y2);
	set_line_cord(x1, y1 + 1 * 1, x2 + table_width, y2 + 1 * 1);
	set_line_cord(x1, y1 + 1 * 2 + height * 1, x2 + table_width,
		      y2 + 1 * 2 + height * 1);
	set_line_cord(x1, y1 + 1 * 3 + height * 2, x2 + table_width,
		      y2 + 1 * 3 + height * 2);
	set_line_cord(x1, y1 + 1 * 4 + height * 3, x2 + table_width,
		      y2 + 1 * 4 + height * 3);
	set_line_cord(x1, y1 + 1 * 4 + height * 3 + 1 * 1, x2 + table_width,
		      y2 + 1 * 4 + height * 3 + 1 * 1);
	set_line_cord(x1, y1 + 1 * 5 + height * 4 + 1 * 1, x2 + table_width,
		      y2 + 1 * 5 + height * 4 + 1 * 1);
	set_line_cord(x1, y1 + 1 * 6 + height * 5 + 1 * 1, x2 + table_width,
		      y2 + 1 * 6 + height * 5 + 1 * 1);
	set_line_cord(x1, y1 + 1 * 7 + height * 6 + 1 * 1, x2 + table_width,
		      y2 + 1 * 7 + height * 6 + 1 * 1);
	set_line_cord(x1, y1 + 1 * 7 + height * 6 + 1 * 2, x2 + table_width,
		      y2 + 1 * 7 + height * 6 + 1 * 2);
	set_line_cord(x1, y1 + 1 * 8 + height * 7 + 1 * 2, x2 + table_width,
		      y2 + 1 * 8 + height * 7 + 1 * 2);
	set_line_cord(x1, y1 + 1 * 9 + height * 8 + 1 * 2, x2 + table_width,
		      y2 + 1 * 9 + height * 8 + 1 * 2);
	set_line_cord(x1, y1 + 1 * 10 + height * 9 + 1 * 2, x2 + table_width,
		      y2 + 1 * 10 + height * 9 + 1 * 2);
	set_line_cord(x1, y1 + 1 * 10 + height * 9 + 1 * 3, x2 + table_width,
		      y2 + 1 * 10 + height * 9 + 1 * 3);

	set_line_cord(x1, y1, x2, y2 + table_height);
	set_line_cord(x1 + 1 * 1, y1, x2 + 1 * 1, y2 + table_height);
	set_line_cord(x1 + 1 * 2 + height * 1, y1, x2 + 1 * 2 + height * 1,
		      y2 + table_height);
	set_line_cord(x1 + 1 * 3 + height * 2, y1, x2 + 1 * 3 + height * 2,
		      y2 + table_height);
	set_line_cord(x1 + 1 * 4 + height * 3, y1, x2 + 1 * 4 + height * 3,
		      y2 + table_height);
	set_line_cord(x1 + 1 * 4 + height * 3 + 1 * 1, y1,
		      x2 + 1 * 4 + height * 3 + 1 * 1, y2 + table_height);
	set_line_cord(x1 + 1 * 5 + height * 4 + 1 * 1, y1,
		      x2 + 1 * 5 + height * 4 + 1 * 1, y2 + table_height);
	set_line_cord(x1 + 1 * 6 + height * 5 + 1 * 1, y1,
		      x2 + 1 * 6 + height * 5 + 1 * 1, y2 + table_height);
	set_line_cord(x1 + 1 * 7 + height * 6 + 1 * 1, y1,
		      x2 + 1 * 7 + height * 6 + 1 * 1, y2 + table_height);
	set_line_cord(x1 + 1 * 7 + height * 6 + 1 * 2, y1,
		      x2 + 1 * 7 + height * 6 + 1 * 2, y2 + table_height);
	set_line_cord(x1 + 1 * 8 + height * 7 + 1 * 2, y1,
		      x2 + 1 * 8 + height * 7 + 1 * 2, y2 + table_height);
	set_line_cord(x1 + 1 * 9 + height * 8 + 1 * 2, y1,
		      x2 + 1 * 9 + height * 8 + 1 * 2, y2 + table_height);
	set_line_cord(x1 + 1 * 10 + height * 9 + 1 * 2, y1,
		      x2 + 1 * 10 + height * 9 + 1 * 2, y2 + table_height);
	set_line_cord(x1 + 1 * 10 + height * 9 + 1 * 3, y1,
		      x2 + 1 * 10 + height * 9 + 1 * 3, y2 + table_height);
}

int game(Element_sudoku *Table_sudoku)
{
	int i, x = 0, y = 0, temp_x = 0, temp_y = 0;
	char key = 0;
	int pos = 0, pos_t = 0;

	get_key_value(); // clear key
	lcd_clear();
	show_table(CORD_X, CORD_Y);

	set_rgb_text(255, 0, 0);
	for (i = 0; i < ROW * COL; ++i) {
		set_rectangle_cord(Table_sudoku[i].width,
				   Table_sudoku[i].height, height, height);
		if (Table_sudoku[i].first != 0) {
			set_xy_ch(Table_sudoku[i].width + 3,
				  Table_sudoku[i].height,
				  Table_sudoku[i].first + 48);
		}
	}
	set_rgb_text(0, 0, 0);

	set_xy_str(185, 20, "A = up");
	set_xy_str(185, 40, "B = down");
	set_xy_str(185, 60, "C = left");
	set_xy_str(185, 80, "D = right");
	set_xy_str(185, 100, "* = newgame");
	set_xy_str(185, 120, "# = exit");
	set_xy_str(100, 200, "Status: game");
	set_rgb_bg(150, 150, 150);
	set_rectangle_cord(Table_sudoku[pos].width, Table_sudoku[pos].height,
			   height, height);
	set_rgb_bg(255, 255, 255);
	lcd_updata();

	while (1) {
		key = get_key_value();
		if (key == 'X')
			continue;
		printf("char = %c\n", key);
		printf("x = %d, y = %d\n", x, y);

		if (key == '#')
			return 0;
		if (key == '*')
			return 1;

		if (key >= 48 && key <= 57) {
			if (Table_sudoku[pos].check == 1) {
				set_rgb_bg(150, 150, 150);
				set_rectangle_cord(Table_sudoku[pos].width,
						   Table_sudoku[pos].height,
						   height, height);
				Table_sudoku[pos].first = key - 48;
				if (key != 48) {
					set_xy_ch(Table_sudoku[pos].width + 3,
						  Table_sudoku[pos].height,
						  Table_sudoku[pos].first + 48);
				}
				set_rgb_bg(255, 255, 255);
				lcd_updata();
			}
			continue;
		}

		if (key == 'A')
			y = (y == 0 ? 0 : y - 1);
		if (key == 'B')
			y = (y == 8 ? 8 : y + 1);
		if (key == 'C')
			x = (x == 0 ? 0 : x - 1);
		if (key == 'D')
			x = (x == 8 ? 8 : x + 1);

		if (key == 'A' || key == 'B' || key == 'C' || key == 'D') {
			pos = (x + 9 * y);
			pos_t = (temp_x + 9 * temp_y);

			set_rgb_bg(150, 150, 150);
			set_rectangle_cord(Table_sudoku[pos].width,
					   Table_sudoku[pos].height, height,
					   height);
			if (Table_sudoku[pos].check != 1) {
				set_rgb_text(255, 0, 0);
			}
			if (Table_sudoku[pos].first != 0) {
				set_xy_ch(Table_sudoku[pos].width + 3,
					  Table_sudoku[pos].height,
					  Table_sudoku[pos].first + 48);
			}
			set_rgb_text(0, 0, 0);
			set_rgb_bg(255, 255, 255);

			if (x != temp_x || y != temp_y) {
				set_rectangle_cord(Table_sudoku[pos_t].width,
						   Table_sudoku[pos_t].height,
						   height, height);
				if (Table_sudoku[pos_t].first != 0) {
					if (Table_sudoku[pos_t].check != 1) {
						set_rgb_text(255, 0, 0);
					}
					if (Table_sudoku[pos_t].first != 0) {
						set_xy_ch(
							Table_sudoku[pos_t]
									.width +
								3,
							Table_sudoku[pos_t]
								.height,
							Table_sudoku[pos_t]
									.first +
								48);
					}
					set_rgb_text(0, 0, 0);
				}
				temp_x = x;
				temp_y = y;
			}
		}

		if (check_win(Table_sudoku)) {
			set_xy_str(100, 200, "Status: win ");
		} else {
			set_xy_str(100, 200, "Status: game");
		}

		lcd_updata();
	}
	return 0;
}

int main(int argc, char *argv[])
{
	int ret = 1;
	size_t game_num = 1;
	Element_sudoku *Table_sudoku =
		malloc((sizeof(Element_sudoku) * ELEMENTS_COUNT));
	init_sudoku_table(Table_sudoku);

	while (ret == 1) {
		init_sudoku_game(Table_sudoku, sudoku_example(game_num++));

		solve_sudoku(Table_sudoku);
		set_rgb_text(0, 0, 0);
		ret = game(Table_sudoku);
	}
	lcd_clear();
	lcd_updata();
	free(Table_sudoku);

	return 0;
}
