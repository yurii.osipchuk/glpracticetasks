#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/uaccess.h>
#include <linux/cdev.h>
// #include <linux/version.h>
#include <linux/jiffies.h>
#include <linux/types.h>
// #include <linux/time.h>

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Yurii Osipchuk");
MODULE_DESCRIPTION("Task07");
MODULE_VERSION("2.0");

#define DIRECTORY "tassk07"

#define CLASS_ATTR(_name, _mode, _show, _store)                                \
	struct class_attribute class_attr_##_name =                            \
		__ATTR(_name, _mode, _show, _store)

static struct class *sys_class;
static u32 time_start;
static u32 set_format = 0;

static ssize_t value_read(struct class *class, struct class_attribute *attr,
			  char *buf)
{
	u32 time_end, temp;
	u32 h, m, s;

	time_end = jiffies;
	if (time_start == 0 || time_end < time_start) {
		temp = 0;
	} else {
		temp = time_end - time_start;
	}
	time_start = jiffies;
	h = ((temp / HZ) / 3600) % 24;
	m = ((temp / HZ) / 60) % 60;
	s = (temp / HZ) % 60;

	if (set_format == 0) {
		printk(KERN_INFO "seconds = %d\n", temp / HZ);
		sprintf(buf, "seconds = %d\n", temp / HZ);
	} else {
		printk(KERN_INFO "%02d:%02d:%02d\n", h, m, s);
		sprintf(buf, "%02d:%02d:%02d\n", h, m, s);
	}

	return strlen(buf);
}

static ssize_t value_write(struct class *class, struct class_attribute *attr,
			   const char *buf, size_t count)
{
	if (buf[0] == '1') {
		set_format = 1;
	}
	return count;
}

CLASS_ATTR(value, (S_IWUSR | S_IRUGO), &value_read, &value_write);

static int __init example_init(void)
{
	int err;
	time_start = 0;

	sys_class = class_create(THIS_MODULE, DIRECTORY);
	if (IS_ERR(sys_class))
		printk("bad class create\n");
	err = class_create_file(sys_class, &class_attr_value);
	if (err)
		goto error;

	printk(KERN_NOTICE "loaded\n");
	return 0;

error:
	printk(KERN_ERR "failed to load\n");
	class_remove_file(sys_class, &class_attr_value);
	class_destroy(sys_class);
	return err;
}

static void __exit example_exit(void)
{
	class_remove_file(sys_class, &class_attr_value);
	class_destroy(sys_class);
	printk(KERN_NOTICE "exited\n");
}

module_init(example_init);
module_exit(example_exit);
