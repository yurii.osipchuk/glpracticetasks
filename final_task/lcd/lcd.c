#include "lcd.h"

//--------------------------------------------------//
//--------------------------------------------------//
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Yurii Osipchuk");
MODULE_DESCRIPTION("LCD");
MODULE_VERSION("1.0");
//--------------------------------------------------//

int str_to_int(const char *str)
{
	size_t tmp, count = 0;
	while (*str) {
		if (*str == ' ' || *str == '\t' || *str == '\n') {
			str++;
			continue;
		}

		if (*str >= 48 && *str <= 57) {
			tmp = *str - 48;
			if (count != 0) {
				count = count * 10 + tmp;
			} else {
				count = tmp;
			}
		}
		str++;
	}
	return count;
}

uint32_t color_color565(const char *str)
{
	uint32_t ret = 0, tmp = 0, count = 0;
	while (*str) {
		if (*str == ' ' || *str == '\t' || *str == '\n') {
			str++;
			continue;
		}
		if (*str == ',') {
			if (count != 0) {
				if (ret == 0) {
					ret = ((count & 0xF8) << 8);
				} else {
					ret |= ((count & 0xFC) << 3);
				}
				count = 0;
			}
			str++;
			continue;
		}

		if (*str >= 48 && *str <= 57) {
			tmp = *str - 48;
			if (count != 0) {
				count = count * 10 + tmp;
			} else {
				count = tmp;
			}
		}
		str++;
	}
	if (count != 0) {
		ret |= ((count & 0xF8) >> 3);
	}

	return ret;
}

FontDef int_font(int num)
{
	if (num == 0) {
		return Font_7x10;
	} else if (num == 1) {
		return Font_11x18;
	} else if (num == 2) {
		return Font_16x26;
	}
	return Font_11x18;
}

void lcd_xy_ch(const char *str, int *x, int *y, char *ch)
{
	int tmp, count = 0, pos = 0;
	while (*str) {
		if (*str == '\t' || *str == '\n') {
			str++;
			continue;
		}

		if (*str == ',') {
			if (pos == 0) {
				*x = count;
				++pos;
			} else if (pos == 1) {
				*y = count;
				++pos;
			}
			count = 0;
			str++;
			continue;
		}

		if (*str >= 48 && *str <= 57 && pos < 2) {
			tmp = *str - 48;
			if (count != 0) {
				count = count * 10 + tmp;
			} else {
				count = tmp;
			}
		}

		if (pos == 2) {
			count = *str;
		}
		str++;
	}
	*ch = (char)count;
}

void lcd_xy_str(const char *str, int *x, int *y, char *str_out)
{
	int tmp, count = 0, pos = 0;
	while (*str) {
		if (*str == '\t' || *str == '\n') {
			str++;
			continue;
		}

		if (*str == ',') {
			if (pos == 0) {
				*x = count;
				++pos;
			} else if (pos == 1) {
				*y = count;
				++pos;
			}
			count = 0;
			str++;
			continue;
		}

		if (*str >= 48 && *str <= 57 && pos < 2) {
			tmp = *str - 48;
			if (count != 0) {
				count = count * 10 + tmp;
			} else {
				count = tmp;
			}
		}

		if (pos == 2) {
			*str_out = *str;
			++str_out;
			*str_out = '\0';
		}
		str++;
	}
}

void lcd_line_x1y1_x2y2(const char *str, int *x1, int *y1, int *x2, int *y2)
{
	int tmp = 0, count = 0, pos = 0;
	while (*str) {
		if (*str == ' ' || *str == '\t' || *str == '\n') {
			str++;
			continue;
		}

		if (*str == ',') {
			if (pos == 0) {
				*x1 = count;
				++pos;
			} else if (pos == 1) {
				*y1 = count;
				++pos;
			} else if (pos == 2) {
				*x2 = count;
				++pos;
			}
			count = 0;
			str++;
			continue;
		}

		if (*str >= 48 && *str <= 57) {
			tmp = *str - 48;
			if (count != 0) {
				count = count * 10 + tmp;
			} else {
				count = tmp;
			}
		}
		str++;
	}
	*y2 = count;
}

//--------------------------------------------------//
static int __init lcd_init(void)
{
	int ret;
	// struct class*
	lcdclass = class_create(THIS_MODULE, CLASS_NAME);
	if (IS_ERR(lcdclass)) {
		printk("=== bad class create\n");
		goto err;
	}

	ret = class_create_file(lcdclass, &class_attr_value);
	if (ret) {
		printk(KERN_ERR "=== Can not class create file value\n");
		goto err;
	}

	ret = class_create_file(lcdclass, &class_attr_str);
	if (ret) {
		printk(KERN_ERR "=== Can not class create file str\n");
		goto err;
	}

	ret = class_create_file(lcdclass,
				&class_attr_text_color); // sys_fs text color
	if (ret) {
		printk(KERN_ERR "=== Can not class create file text color\n");
		goto err;
	}

	ret = class_create_file(lcdclass,
				&class_attr_bg_color); // sys_fs bg color
	if (ret) {
		printk(KERN_ERR "=== Can not class create file bg color\n");
		goto err;
	}

	ret = class_create_file(lcdclass, &class_attr_font);
	if (ret) {
		printk(KERN_ERR "=== Can not class create file font\n");
		goto err;
	}

	ret = class_create_file(lcdclass, &class_attr_line);
	if (ret) {
		printk(KERN_ERR "=== Can not class create file line\n");
		goto err;
	}

	ret = class_create_file(lcdclass, &class_attr_updata);
	if (ret) {
		printk(KERN_ERR "=== Can not class create updata\n");
		goto err;
	}

	ret = class_create_file(lcdclass, &class_attr_clear);
	if (ret) {
		printk(KERN_ERR "=== Can not class create clear\n");
		goto err;
	}

	ret = class_create_file(lcdclass, &class_attr_rectangle);
	if (ret) {
		printk(KERN_ERR "=== Can not class create rectangle\n");
		goto err;
	}

	ret = start_func();
	if (ret != 0) {
		goto err;
	}

	printk(KERN_INFO
	       "=============== lcd module installed ===============\n");
	return 0;

err:
	return ret;
}

static void __exit lcd_exit(void)
{
	class_remove_file(lcdclass, &class_attr_value);
	class_destroy(lcdclass);
	exit_func();
	printk(KERN_INFO
	       "=============== lcd module removed ================\n");
}
//--------------------------------------------------//
module_init(lcd_init);
module_exit(lcd_exit);
//--------------------------------------------------//
