#include <stdio.h>
#include <stdlib.h>
#include <time.h>

double test_malloc(size_t count){
	clock_t time_start = 0, time_end = 0;
	double time_used = 0;
	char *buffer = NULL;
	time_start = clock();
	buffer = malloc(count);
	if(buffer != NULL){
		buffer[0] = 1;
		buffer[count-1] = 1;
		free(buffer);
	} else {
		return -1;
	}
	time_end = clock();
	time_used = ((double) (time_end - time_start)) / CLOCKS_PER_SEC;
	return time_used;
}

double test_calloc(size_t count){
	clock_t time_start = 0, time_end = 0;
	double time_used = 0;
	char *buffer = NULL;
	time_start = clock();
	buffer = calloc(count, sizeof(char));
	if(buffer != NULL){
		buffer[0] = 1;
		buffer[count-1] = 1;
		free(buffer);
	} else {
		return -1;
	}
	time_end = clock();
	time_used = ((double) (time_end - time_start)) / CLOCKS_PER_SEC;
	return time_used;
}

double test_alloac(size_t count){
	clock_t time_start = 0, time_end = 0;
	double time_used = 0;
	char *buffer = NULL;
	time_start = clock();
	buffer = alloca(count);
	if(buffer != NULL){
		buffer[0] = 1;
		buffer[count-1] = 1;
	} else {
		return -1;
	}
	time_end = clock();
	time_used = ((double) (time_end - time_start)) / CLOCKS_PER_SEC;
	return time_used;
}

int main(int argc, char* argv[])
{
	size_t i;

	printf("| bytes/seconds ================================= |\n");
	printf("| ============== |  malloc  |  calloc  |  alloca  |\n");
	printf("|*************** |**********|**********|**********|\n");

	for (i = 1; i <= 4194304; i *= 2)
	{
		printf("| %14ld | %f | %f | %f |\n", i, test_malloc(i), test_calloc(i), test_alloac(i));
		// printf("[malloc]: %ld bytes = %f seconds\n", i, test_malloc(i));
		// printf("[calloc]: %ld bytes = %f seconds\n", i, test_calloc(i));
		// printf("[alloca]: %ld bytes = %f seconds\n", i, test_alloac(i));
	}

	return 0;
}

