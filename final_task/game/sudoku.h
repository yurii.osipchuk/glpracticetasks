#ifndef __SUDOKU_H__
#define __SUDOKU_H__

#include <stdio.h>

#define CORD_X 5
#define CORD_Y 5
#define ROW 9
#define COL 9
#define ELEMENTS_COUNT 81

typedef struct {
	int width;
	int height;
	int first;
	int second;
	int check;
} Element_sudoku;

void init_sudoku_table(Element_sudoku *Table_sudoku);
void init_sudoku_game(Element_sudoku *Table_sudoku, int *example);
void print_table(Element_sudoku *Table_sudoku);
void print_solve_table(Element_sudoku *Table_sudoku);
int solve_sudoku(Element_sudoku *Table_sudoku);
int *sudoku_example(int num);
int check_win(Element_sudoku *Table_sudoku);

#endif // __SUDOKU_H__