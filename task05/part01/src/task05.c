#include <linux/module.h>
#include <linux/moduleparam.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Yurii Osipchuk");
MODULE_VERSION("0.01");
MODULE_DESCRIPTION("Currency converter");

static int kurs = 0;
module_param(kurs, int, 0);

static int UAH = 0;
module_param(UAH, int, 0);

static void ua_to_eur(int UAH, int kurs)
{
	printk(KERN_INFO "Currency converter:\n");
	printk(KERN_INFO "kurs = %d\n", kurs);
	printk(KERN_INFO "UAH = %d\n", UAH);
	printk(KERN_INFO "UAH/EUR = %d.%02d\n", UAH / kurs, UAH % kurs);
}

static int __init mod_init(void)
{
	ua_to_eur(UAH, kurs);
	return -1;
}

module_init(mod_init);
