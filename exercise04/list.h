#ifndef LIST_H
#define LIST_H

#include <stdio.h>
#include <stdlib.h>

struct Node {
	int key;
	char *name;
	int result;
	struct Node *next;
	struct Node *prev;
};

void push_list(struct Node **head_ref, int key, char *name);
struct Node *find_start(struct Node *head_ref);
struct Node *find_end(struct Node *head_ref);
void print_list(struct Node *head_ref);
void set_list(struct Node *head_ref, int key, int result);
int get_list_result(struct Node *head_ref, int key);
char *get_list_name(struct Node *head_ref, int key);
void close_list(struct Node *head_ref);

#endif