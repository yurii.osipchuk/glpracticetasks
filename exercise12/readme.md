# Exercise #12

```bash
# Install headers
sudo apt-get install raspberrypi-kernel-headers

# DTC – Device tree compiler
dtc -v

# Enable I2C bus
sudo raspi-config

# Compile device tree
dtc -I dts -O dtb -o my_ds3231.dtbo my_ds3231.dts

# Copy output file
sudo cp my_ds3231.dtbo /boot/overlays/

# Enable overlay
# Add line “dtoverlay=my_ds3231”
sudo nano /boot/config.txt

# Reboot device
sudo reboot

# Check overlay
fdtdump my_ds3231.dtbo
sudo vcdbg log msg
dtc -I fs /proc/device-tree
```
