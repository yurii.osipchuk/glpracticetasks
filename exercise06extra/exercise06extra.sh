#!/bin/bash

trap "CLEAR; exit" SIGINT

PIN_BUTTON=26
PIN_RED_LED=20
PIN_GREEN_LED=21
PIN_BLUE_LED=16
START=0
BUTTON_DETECT=0

echo ${PIN_BUTTON} > /sys/class/gpio/export
echo ${PIN_RED_LED} > /sys/class/gpio/export
echo ${PIN_GREEN_LED} > /sys/class/gpio/export
echo ${PIN_BLUE_LED} > /sys/class/gpio/export

echo in > /sys/class/gpio/gpio${PIN_BUTTON}/direction
echo out > /sys/class/gpio/gpio${PIN_RED_LED}/direction
echo out > /sys/class/gpio/gpio${PIN_GREEN_LED}/direction
echo out > /sys/class/gpio/gpio${PIN_BLUE_LED}/direction


PRINT_RGB(){
    echo "1" > /sys/class/gpio/gpio${PIN_RED_LED}/value
    sleep 0.5
    echo "0" > /sys/class/gpio/gpio${PIN_RED_LED}/value
    echo "1" > /sys/class/gpio/gpio${PIN_GREEN_LED}/value
    sleep 0.5
    echo "0" > /sys/class/gpio/gpio${PIN_GREEN_LED}/value
    echo "1" > /sys/class/gpio/gpio${PIN_BLUE_LED}/value
    sleep 0.5
    echo "0" > /sys/class/gpio/gpio${PIN_BLUE_LED}/value
}

PRINT_BGR(){
    echo "1" > /sys/class/gpio/gpio${PIN_BLUE_LED}/value
    sleep 0.5
    echo "0" > /sys/class/gpio/gpio${PIN_BLUE_LED}/value
    echo "1" > /sys/class/gpio/gpio${PIN_GREEN_LED}/value
    sleep 0.5
    echo "0" > /sys/class/gpio/gpio${PIN_GREEN_LED}/value
    echo "1" > /sys/class/gpio/gpio${PIN_RED_LED}/value
    sleep 0.5
    echo "0" > /sys/class/gpio/gpio${PIN_RED_LED}/value
}

CLEAR(){
    echo "0" > /sys/class/gpio/gpio${PIN_RED_LED}/value
    echo "0" > /sys/class/gpio/gpio${PIN_GREEN_LED}/value
    echo "0" > /sys/class/gpio/gpio${PIN_BLUE_LED}/value
    echo ${PIN_BUTTON} > /sys/class/gpio/unexport
    echo ${PIN_RED_LED} > /sys/class/gpio/unexport
    echo ${PIN_GREEN_LED} > /sys/class/gpio/unexport
    echo ${PIN_BLUE_LED} > /sys/class/gpio/unexport
}

while [ $START -eq 0 ]; do
    BUTTON_DETECT=$(cat /sys/class/gpio/gpio${PIN_BUTTON}/value)
    if [ $BUTTON_DETECT -eq 1 ]; then
        START=1
    fi
done

while true; do
    BUTTON_DETECT=$(cat /sys/class/gpio/gpio${PIN_BUTTON}/value)
    if [ $BUTTON_DETECT -eq 1 ]; then
        START=$(($START + 1))
    fi
    if [ $(($START%2)) == 0 ]; then
         PRINT_RGB
    fi

    if [ $(($START%2)) == 1 ]; then
        PRINT_BGR
    fi
done

# echo ${PIN_BUTTON} > /sys/class/gpio/unexport
# echo ${PIN_RED_LED} > /sys/class/gpio/unexport
# echo ${PIN_GREEN_LED} > /sys/class/gpio/unexport
# echo ${PIN_BLUE_LED} > /sys/class/gpio/unexport

