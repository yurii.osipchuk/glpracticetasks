#!/bin/bash

# make
sudo dmesg -C
sudo insmod task08.ko
cat /sys/class/tassk08/value
echo "123" | sudo tee /sys/class/tassk08/value
cat /sys/class/tassk08/value
echo "john|asd0123456789|5" | sudo tee /sys/class/tassk08/value
cat /sys/class/tassk08/value
cat /sys/class/tassk08/user
echo "Nic" | sudo tee /sys/class/tassk08/user
cat /sys/class/tassk08/user
echo "Nic|msg asd|1" | sudo tee /sys/class/tassk08/value
echo "Nic|msg aaaaaaaaaa|1" | sudo tee /sys/class/tassk08/value
echo "john|msg xxxxxxxxx|1" | sudo tee /sys/class/tassk08/value
echo "Nic|msg bbbbbbbbbb|1" | sudo tee /sys/class/tassk08/value
echo "Max|msg qwerty|7" | sudo tee /sys/class/tassk08/value
sleep 1
cat /sys/class/tassk08/value
sleep 4
cat /sys/class/tassk08/value
sleep 4
cat /sys/class/tassk08/value
cat /sys/class/tassk08/value
sudo rmmod task08.ko
dmesg
# make clean

sudo dmesg -C
sudo insmod task08.ko
cat /sys/class/tassk08/value
echo "123" | sudo tee /sys/class/tassk08/value
cat /sys/class/tassk08/value
echo "john|asd0123456789|5" | sudo tee /sys/class/tassk08/value
cat /sys/class/tassk08/value
cat /sys/class/tassk08/user
echo "Nic2" | sudo tee /sys/class/tassk08/user
cat /sys/class/tassk08/user
echo "Nic|msg asd|1" | sudo tee /sys/class/tassk08/value
echo "Nic|msg aaaaaaaaaa|1" | sudo tee /sys/class/tassk08/value
echo "john|msg xxxxxxxxx|1" | sudo tee /sys/class/tassk08/value
echo "Nic|msg bbbbbbbbbb|1" | sudo tee /sys/class/tassk08/value
echo "Max|msg qwerty|7" | sudo tee /sys/class/tassk08/value
sleep 1
cat /sys/class/tassk08/value
sleep 4
cat /sys/class/tassk08/value
sleep 4
cat /sys/class/tassk08/value
cat /sys/class/tassk08/value
sudo rmmod task08.ko
dmesg

sudo dmesg -C
sudo insmod task08.ko
cat /sys/class/tassk08/value
echo "123" | sudo tee /sys/class/tassk08/value
cat /sys/class/tassk08/value
echo "john|asd0123456789|5" | sudo tee /sys/class/tassk08/value
cat /sys/class/tassk08/value
cat /sys/class/tassk08/user
echo "Nic" | sudo tee /sys/class/tassk08/user
cat /sys/class/tassk08/user
echo "Nic|msg asd|1" | sudo tee /sys/class/tassk08/value
echo "Nic|msg aaaaaaaaaa|1" | sudo tee /sys/class/tassk08/value
echo "john|msg xxxxxxxxx|1" | sudo tee /sys/class/tassk08/value
echo "Nic|msg bbbbbbbbbb|1" | sudo tee /sys/class/tassk08/value
echo "Max|msg qwerty|7" | sudo tee /sys/class/tassk08/value
sleep 1
cat /sys/class/tassk08/status
echo "1" | sudo tee /sys/class/tassk08/status
cat /sys/class/tassk08/status
cat /sys/class/tassk08/value
sleep 4
cat /sys/class/tassk08/value
sleep 4
cat /sys/class/tassk08/value
cat /sys/class/tassk08/value
sudo rmmod task08.ko
dmesg

