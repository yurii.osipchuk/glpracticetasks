# Author

- Yurii Osipchuk
- Kiev

# Title

Game Sudoku

# Description

1. Development of kernel modules lcd and matrix keyboard
2. Equipment: raspberry pi zero, 3.2" 320x240 display, 4x4 matrix keyboard

## Equipment

1. ![Raspberry pi zero](res/raspberry-pi-zero-w.1.jpg)
2. ![Display 3.2 (320×240)](res/3.2lcd.jpg)
3. ![Matrix Keypad](res/Matrix-Keypad.jpg)


## Interface

- matrix keyboard module:

```text
char get_key_value(void);
/dev/key
return 0123456789ABCD*# if button push or X if not button push
```

- lcd module:

```text
int lcd_updata(void);
/sys/class/lcd/updata
write lcd buffer in lcd module

int lcd_clear(void);
/sys/class/lcd/clear
clear lcd module

int set_font(int value);
/sys/class/lcd/font
set font lcd

int set_line_cord(int x1, int y1, int x2, int y2);
/sys/class/lcd/line
write line in buffer, (x1, y1) first point and (x2, y2) second point

int set_rectangle_cord(int x1, int y1, int x2, int y2);
/sys/class/lcd/rectangle
write rectangle in buffer, (x1, y1) first point and (x2, y2) second point

int set_rgb_bg(int r, int g, int b);
/sys/class/lcd/bg_color
set color background

int set_rgb_text(int r, int g, int b);
/sys/class/lcd/text_color
set color text

int set_xy_ch(int x, int y, int ch);
/sys/class/lcd/value
write char in buffer, x coordinate and y coordinate 

int set_xy_str(int x, int y, char *str);
/sys/class/lcd/str
write string in buffer, x coordinate and y coordinate 
```


## Game

Start:
1. ![Start](res/pic01.jpg)

New Game:
2. ![New Game](res/pic02.jpg)

Game:
3. ![New Game](res/pic03.jpg)

Game:
4. ![New Game](res/pic04.jpg)

Win:
5. ![New Game](res/pic05.jpg)
