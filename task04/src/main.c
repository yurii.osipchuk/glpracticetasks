#include "ili9341.h"
#include <time.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>

#define KEY_1 18
#define KEY_2 23
#define KEY_3 24

void get_ip(char *ip_address)
{
	int fd;
	struct ifreq ifr;
	int i;

	fd = socket(AF_INET, SOCK_DGRAM, 0);
	ifr.ifr_addr.sa_family = AF_INET;

	memcpy(ifr.ifr_name, "wlan0", IFNAMSIZ - 1);

	ioctl(fd, SIOCGIFADDR, &ifr);
	close(fd);

	strcpy(ip_address,
	       inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));
}

void get_time(char *str)
{
	struct tm *u;
	time_t timer = time(NULL);
	u = localtime(&timer);
	strftime(str, 80, "%d.%m.%Y %H:%M:%S ", u);
}

void show_lcd(int count)
{
	int x = (LCD_WIDTH - 11 - 1) / 2;
	int y = (LCD_HEIGHT - 18 - 1) / 2;

	char str_count[12] = { 0 };
	int len;

	char str_time[40] = { 0 };
	char ip[15] = { 0 };

	int i = 0;
	get_time(str_time);

	len = snprintf(str_count, sizeof(str_count), "%d", count);

	lcd_put_str(x, y, "    ", Font_11x18, COLOR_BLACK, COLOR_BLUE);

	lcd_put_str(x, y, str_count, Font_11x18, COLOR_BLACK, COLOR_BLUE);

	len = strlen(str_time);
	int a = LCD_WIDTH - (len * 11) - 1;
	lcd_put_str(a, 0, str_time, Font_11x18, COLOR_BLACK, COLOR_BLUE);

	get_ip(ip);
	lcd_put_str(0, (LCD_HEIGHT - 18 - 1), ip, Font_11x18, COLOR_BLACK,
		    COLOR_BLUE);

	lcd_update_screen();
}

int main(int argc, char *argv[])
{
	gpio_init(GPIO_PIN_RESET);
	gpio_init(GPIO_PIN_DC);
	// gpio_init(GPIO_PIN_CS);
	gpio_init_button(KEY_1);
	gpio_init_button(KEY_2);
	gpio_init_button(KEY_3);

	lcd_reset();
	gpio_init_device();
	lcd_init_ili9341();
	printf("LCD init ok\n");
	lcd_set_address_window(0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1);
	lcd_fill_screen(COLOR_BLUE);

	int count = 0;
	while (1) {
		show_lcd(count);

		if (!gpio_detect_key(KEY_1)) {
			++count;
			while (!gpio_detect_key(KEY_1)) {
				show_lcd(count);
			}
		}
		if (!gpio_detect_key(KEY_2)) {
			count = 0;
			time_t t1 = time(NULL);
			time_t t2;
			while (!gpio_detect_key(KEY_2)) {
				show_lcd(count);
			}
			t2 = time(NULL);
			if ((t1 + 2) <= t2) {
				break;
			}
		}
		if (!gpio_detect_key(KEY_3)) {
			--count;
			while (!gpio_detect_key(KEY_3)) {
				show_lcd(count);
			}
		}
		show_lcd(count);
	}

	lcd_fill_screen(COLOR_BLUE);
	int x = (LCD_WIDTH - 11) / 2;
	int y = (LCD_HEIGHT - 18) / 2;
	lcd_put_str(x - 22, y, "EXIT", Font_11x18, COLOR_BLACK, COLOR_BLUE);
	lcd_update_screen();

	sleep(2);
	gpio_free_device();

	gpio_free(GPIO_PIN_RESET);
	gpio_free(GPIO_PIN_DC);
	// gpio_free(GPIO_PIN_CS);

	gpio_free(KEY_1);
	gpio_free(KEY_2);
	gpio_free(KEY_3);

	return EXIT_SUCCESS;
}
