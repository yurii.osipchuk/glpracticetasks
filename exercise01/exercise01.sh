#!/bin/bash

ARG1=$1
ARG2=$2

print_help()
{
  echo "Usage: $0 = current folder"
  echo "Usage: $0 <folder>"
  echo "Usage: $0 [options]"
  echo "Options:"
  echo " -h, --help - print help"
  echo " -r, --recursive - remove directories and their contents recursively"
  echo " -y, --yes - ???"
  echo " -t, --test - display find file"
}

find_file()
{
  if [ -z $ARG2 ]; then
    for i in $(find $(pwd) -name "[~_-]*") $(find $(pwd) -name "*.tmp"); do
      echo $i;
    done
  else
    for i in $(find $ARG2 -name "[~_-]*") $(find $ARG2 -name "*.tmp"); do
      echo $i;
    done
  fi
}

find_folder()
{
  if [ -z $ARG2 ]; then
    for i in $(find $(pwd) -mindepth 1 -type d -empty); do
      echo $i;
    done
  else
    for i in $(find $ARG2 -mindepth 1 -type d -empty); do
      echo $i;
    done
  fi
}

delet_recursive()
{
  rm -rf $(find_file);
  rm -rf $(find_folder);
}

case $ARG1 in
  "") rm -rf $(pwd)/[-_~]* $(pwd)/*.tmp;;
  "-h"|"--help") print_help;;
  "-r"|"--recursive") delet_recursive;;
  "-y"|"--yes") echo "yes";;
  "-t"|"--test") find_file;;
  *) rm -rf $ARG1/[-_~]* $ARG1/*.tmp;;
esac
