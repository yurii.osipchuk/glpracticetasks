#include "interface.h"

char get_key_value(void)
{
	int valuefd;
	char ch = 'X';
	valuefd = open("/dev/key", O_RDONLY);
	if (valuefd < 0) {
		perror("error open:");
		return ch;
	}
	read(valuefd, &ch, 1);
	close(valuefd);
	return ch;
}

int int_char(int ch)
{
	if (ch == 0) {
		return 32;
	}

	if (ch >= 1 && ch <= 9) {
		return (ch + 48);
	}
	return 32;
}

int lcd_updata(void)
{
	int valuefd;
	char ch;
	valuefd = open("/sys/class/lcd/updata", O_RDONLY);
	if (valuefd < 0) {
		perror("error open:");
		return -1;
	}
	read(valuefd, &ch, 1);
	close(valuefd);
	return 0;
}

int lcd_clear(void)
{
	int valuefd;
	char ch;
	valuefd = open("/sys/class/lcd/clear", O_RDONLY);
	if (valuefd < 0) {
		perror("error open:");
		return -1;
	}
	read(valuefd, &ch, 1);
	close(valuefd);
	return 0;
}

int set_font(int value)
{
	int valuefd;
	// 0 = Font_7x10;
	// 1 = Font_11x18;
	// 2 = Font_16x26;
	char temp = '0';
	if (value >= 0 && value <= 2) {
		temp = temp + value;
	} else {
		temp = '1';
	}

	valuefd = open("/sys/class/lcd/font", O_WRONLY);
	if (valuefd < 0) {
		perror("error open font:");
		return -1;
	}
	write(valuefd, &temp, 1);
	close(valuefd);
	return 0;
}

int set_line_cord(int x1, int y1, int x2, int y2)
{
	int valuefd;
	char buf[32];
	sprintf(buf, "%d, %d, %d, %d", x1, y1, x2, y2);
	valuefd = open("/sys/class/lcd/line", O_WRONLY);
	if (valuefd < 0) {
		perror("error set line:");
		return -1;
	}
	write(valuefd, buf, strlen(buf));
	close(valuefd);
	return 0;
}

int set_rectangle_cord(int x1, int y1, int x2, int y2)
{
	int valuefd;
	char buf[32];
	sprintf(buf, "%d, %d, %d, %d", x1, y1, x2, y2);
	valuefd = open("/sys/class/lcd/rectangle", O_WRONLY);
	if (valuefd < 0) {
		perror("error set line:");
		return -1;
	}
	write(valuefd, buf, strlen(buf));
	close(valuefd);
	return 0;
}

int set_rgb_bg(int r, int g, int b)
{
	int valuefd;
	char buf[23];
	sprintf(buf, "%d, %d, %d", r, g, b);
	valuefd = open("/sys/class/lcd/bg_color", O_WRONLY);
	if (valuefd < 0) {
		perror("error set bg_color:");
		return -1;
	}
	write(valuefd, buf, strlen(buf));
	close(valuefd);
	return 0;
}

int set_rgb_text(int r, int g, int b)
{
	int valuefd;
	char buf[32];
	sprintf(buf, "%d, %d, %d", r, g, b);
	valuefd = open("/sys/class/lcd/text_color", O_WRONLY);
	if (valuefd < 0) {
		perror("error set text_color:");
		return -1;
	}
	write(valuefd, buf, strlen(buf));
	close(valuefd);
	return 0;
}

int set_xy_ch(int x, int y, int ch)
{
	int valuefd;
	char buf[32];
	sprintf(buf, "%d, %d, %c", x, y, (char)ch);
	valuefd = open("/sys/class/lcd/value", O_WRONLY);
	if (valuefd < 0) {
		perror("error set value:");
		return -1;
	}
	write(valuefd, buf, strlen(buf));
	close(valuefd);
	return 0;
}

int set_xy_str(int x, int y, char *str)
{
	int valuefd;
	char buf[256];
	sprintf(buf, "%d, %d, %s", x, y, str);
	valuefd = open("/sys/class/lcd/str", O_WRONLY);
	if (valuefd < 0) {
		perror("error set value:");
		return -1;
	}
	write(valuefd, buf, strlen(buf));
	close(valuefd);
	return 0;
}
